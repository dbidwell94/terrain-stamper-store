import { Configuration, WebpackPluginInstance } from 'webpack';
import tsConfigPathPlugin from 'tsconfig-paths-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import compression from 'compression-webpack-plugin';
import terser from 'terser-webpack-plugin';
import path from 'path';

const productionPlugins: WebpackPluginInstance[] = [
  new compression({
    algorithm: 'brotliCompress',
    filename: '[path][base].br',
  }),
  new compression({
    algorithm: 'gzip',
    filename: '[path][base].gz',
  }),
  new HtmlWebpackPlugin({
    template: './public/index.html',
    title: 'Terrain Stamper Store',
    favicon: 'src/assets/favicon.png',
    inject: 'head',
    publicPath: '/',
  }),
];

const devPlugins: WebpackPluginInstance[] = [
  new HtmlWebpackPlugin({
    template: './public/index.html',
    title: 'Terrain Stamper Store',
    inject: 'head',
    favicon: 'src/assets/favicon.png',
    publicPath: '/',
    hash: true,
  }),
];

const config = (env: any, args: any): Configuration => {
  const isProduction: boolean = args.mode === 'production';
  return {
    entry: 'src/index.tsx',
    mode: isProduction ? 'production' : 'development',
    module: {
      rules: [
        {
          test: /\.(tsx?|jsx?)$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript'],
            },
          },
        },
        {
          test: /\.html$/,
          use: 'html-loader',
        },
        {
          test: /\.(css|less)$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          loader: 'file-loader',
          options: {
            name: 'public/icons/[name].[ext]',
          },
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.css', '.less', '.json'],
      plugins: [new tsConfigPathPlugin({ configFile: 'tsconfig.json' })] as any,
    },
    plugins: isProduction ? productionPlugins : devPlugins,
    devServer: {
      historyApiFallback: true,
      port: 2021,
      hot: true,
      open: true,
      overlay: true,
    },
    output: {
      path: path.join(__dirname, 'dist'),
      // filename: isProduction ? '[chunkhash].bundle.js' : '[name].js',
    },
    cache: {
      type: 'memory',
    },
    devtool: 'nosources-source-map',
    optimization: isProduction
      ? {
          minimize: true,
          minimizer: [
            new terser({
              extractComments: true,
              terserOptions: {
                keep_classnames: false,
                compress: {
                  dead_code: true,
                  drop_console: true,
                  drop_debugger: true,
                  hoist_funs: true,
                },
                mangle: true,
              },
            }),
          ],
        }
      : {},
    performance: {
      maxEntrypointSize: 500000,
    },
  };
};

export default config;
