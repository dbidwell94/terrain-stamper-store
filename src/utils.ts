import { IUser } from './api/userApi';
import _ from 'lodash';

/**
 *
 * @param user The user in which to perform the check on
 * @returns A boolean defining if the user has admin privileges
 */
export function isAdmin(user: IUser | null): boolean {
  if (!user) {
    return false;
  }
  const userIsAdmin = user.roles.reduce((currentlyIsAdmin, role) => {
    if (!currentlyIsAdmin) {
      if (role.roleName.toLowerCase().includes('admin')) return true;
    }
    return currentlyIsAdmin;
  }, false);

  return userIsAdmin;
}

export function arrayEquals<T>(a: T[], b: T[]): boolean {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;
  if (typeof a !== typeof b) return false;

  for (let i = 0; i < a.length; ++i) {
    if (typeof a[i] === 'object' && typeof b[i] === 'object') {
      if (!_.isEqual(a[i], b[i])) return false;
    } else if (a[i] !== b[i]) return false;
  }
  return true;
}
