import React from 'react';
import styled from 'styled-components';

const DownloadProgressContainer = styled.div``;

interface IDownloadProgressProps {
  /**
   * How far into the download are we? 0% - 100% (as an integer between 0 - 1)
   */
  progress?: number;
}

export default function DownloadProgress(_: IDownloadProgressProps) {
  return <DownloadProgressContainer></DownloadProgressContainer>;
}
