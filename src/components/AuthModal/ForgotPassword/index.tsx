import React, { Dispatch, SetStateAction, useState } from 'react';
import { useSelector } from 'react-redux';
import api from '@api/index';
import Button from '@sc/Button';
import Input, { inputTypes } from '@sc/Input';
import { IFormNames } from '../index';
import * as yup from 'yup';
import { ApiError } from '@api/apiClient';

const emailSchema = yup.string().email('Badly Formatted E-Mail Address.').required('This field is required.');

interface IForgotPasswordProps {
  setCurrentAuthForm: Dispatch<SetStateAction<IFormNames>>;
}

export default function ForgotPassword(props: IForgotPasswordProps) {
  const { isLoading } = useSelector((state) => state.authReducer);
  const [submitted, setSubmitted] = useState(false);
  const [addressToSendTo, setAddressToSendTo] = useState('');
  const [emailError, setEmailError] = useState('');
  const [serverError, setServerError] = useState<null | string>(null);

  async function onSubmit(evt: React.FormEvent<HTMLFormElement>) {
    evt.preventDefault();
    try {
      const email = await emailSchema.validate(addressToSendTo, { abortEarly: false });
      setSubmitted(true);
      await api.userApi.sendForgotPasswordEmail(email);
    } catch (err) {
      if (err instanceof yup.ValidationError) {
        setServerError(err.message);
      } else if (err instanceof ApiError) {
        setServerError(err.message);
      } else {
        setServerError('An unknown error has occurred');
      }
      setSubmitted(false);
    }
  }

  async function onChange(evt: React.ChangeEvent<HTMLInputElement>) {
    evt.preventDefault();

    const { value } = evt.target;
    setAddressToSendTo(value);

    try {
      await emailSchema.validate(value);
    } catch (err) {
      if (err instanceof yup.ValidationError) {
        setEmailError(err.message);
        return;
      }
    }

    setEmailError('');
  }

  return (
    <form onSubmit={onSubmit}>
      {submitted && (
        <span className='server-success'>
          <p>A link to reset your password has been sent to your email</p>
        </span>
      )}
      {serverError && (
        <span className='server-error'>
          <p>{serverError}</p>
        </span>
      )}
      <span>
        <label htmlFor='email'>Email</label>
        <Input
          type={inputTypes.email}
          name='email'
          id='email'
          placeholder='you@email.com'
          value={addressToSendTo}
          onChange={onChange}
          disabled={submitted}
          errorText={emailError}
        />
      </span>
      <div className='registerforgotpass'>
        <a onClick={() => props.setCurrentAuthForm('login')} className='button'>
          Login
        </a>
        <a onClick={() => props.setCurrentAuthForm('register')} className='button'>
          Register
        </a>
      </div>
      <div>
        <Button displayText='Submit' type='submit' disabled={isLoading || submitted} />
      </div>
    </form>
  );
}
