import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import Login from './Login';
import Register from './Register';
import { toggleAuthModal } from '@state/AuthReducer/AuthActions';
import Button from '@sc/Button';
import ForgotPassword from './ForgotPassword';

interface IAuthStyles {
  borderRadius: number;
  background: string;
  inactiveBackground: string;
}

export const AuthContainer = styled.div<IAuthStyles>`
  z-index: 20;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  width: calc(100vw * 0.25);
  min-width: 45rem;
  border: 3px solid #494949;
  background: #1f1f1f;
  display: flex;
  flex-direction: column;

  div.authboxhead {
    width: 100%;
    color: white;
    font-weight: bold;
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 20pt;
    background: #303030;
    padding: 20px;
  }

  div.content {
    width: 100%;
    height: 100%;

    span.server-error {
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      border-bottom: thin solid red;
      border-top: thin solid red;
      padding: 1rem;
      p {
        font-size: 1.5rem;
      }
    }
    span.server-success {
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      border-bottom: thin solid green;
      border-top: thin solid green;
      padding: 1rem;
      p {
        font-size: 1.5rem;
      }
    }

    form {
      height: 100%;
      padding: 50px 80px;

      span {
        width: 100%;
        display: grid;
        grid-gap: 5px;

        label {
          color: gray;
          text-transform: uppercase;
        }

        input {
          width: 100%;
          height: 50px;
          border: 2px solid transparent;
        }

        input:hover {
          border: 2px solid gray;
        }

        input:focus {
          border: 2px solid #0087ff;
        }

        div {
          margin-bottom: 50px;
        }
      }

      div {
        width: 100%;
      }

      .submit {
        margin: 0 auto;
        width: 150px;
      }

      .registerforgotpass {
        display: flex;
        flex-direction: row;
        height: 50px;
        border-bottom: 2px solid #000000;
        justify-content: space-evenly;
        width: 100%;
        margin: 0 auto;
        margin-bottom: 50px;

        a {
          font-size: 11pt;
          padding: 5px;
          color: #0087ff;
          &.button {
            cursor: pointer;
          }
        }
      }

      .headtext {
        padding: 25px;
        display: block;
        color: gray;
        text-align: center;
        width: 100%;
        text-transform: uppercase;
        margin: 0 auto;
        margin-bottom: 50px;
        border-bottom: 2px solid #000000;
      }
      .confirmation {
        margin-bottom: ${({ theme }) => theme.global.padding}rem;
      }
    }
  }
`;

export const ParentAuthContainer = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 20;
`;

const formTitles = {
  login: 'SIGN IN',
  register: 'REGISTER',
  forgotPassword: 'FORGOT PASSWORD',
};

export type IFormNames = keyof typeof formTitles;

export default function AuthModal() {
  const [currentFormItem, setCurrentFormItem] = useState<IFormNames>('login');

  const dispatch = useDispatch();
  const { errors: serverErrors } = useSelector((state) => state.authReducer);

  function getAuthForm(): JSX.Element {
    switch (currentFormItem) {
      case 'login':
        return <Login setCurrentAuthForm={setCurrentFormItem} />;
      case 'register':
        return <Register setCurrentAuthForm={setCurrentFormItem} />;
      case 'forgotPassword':
        return <ForgotPassword setCurrentAuthForm={setCurrentFormItem} />;
      default:
        return <></>;
    }
  }

  return (
    <ParentAuthContainer
      id='cancel-modal-event-container'
      onClick={(e) => {
        e.preventDefault();
        if (e.currentTarget.id === 'cancel-modal-event-container') {
          dispatch(toggleAuthModal(false));
        }
      }}
    >
      <AuthContainer
        borderRadius={1.5}
        background='white'
        inactiveBackground='grey'
        onClick={(e) => e.stopPropagation()}
      >
        <div className='authboxhead'>
          {formTitles[currentFormItem]}
          <Button displayText='Close' onClick={() => dispatch(toggleAuthModal(false))} />
        </div>

        <div className='content'>
          {serverErrors && serverErrors.length > 0 && (
            <span className='server-error'>
              <p>{serverErrors[0].message}</p>
            </span>
          )}
          {currentFormItem && getAuthForm()}
        </div>
      </AuthContainer>
    </ParentAuthContainer>
  );
}
