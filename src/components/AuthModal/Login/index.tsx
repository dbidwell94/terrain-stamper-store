import React, { Dispatch, SetStateAction } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@sc/Button';
import Input, { inputTypes } from '@sc/Input';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { logUserIn } from '@state/AuthReducer/AuthActions';
import { IFormNames } from '..';

const formSchema = yup.object().shape({
  username: yup.string().required('Username is required'),
  password: yup.string().required('Password is required'),
});

interface IFormValues {
  username: string;
  password: string;
}

interface ILoginProps {
  setCurrentAuthForm: Dispatch<SetStateAction<IFormNames>>;
}

export default function Login(props: ILoginProps) {
  const { isLoading } = useSelector((state) => state.authReducer);
  const dispatch = useDispatch();

  const { register, handleSubmit, errors } = useForm<IFormValues>({ resolver: yupResolver(formSchema) });

  function onSubmit(values: IFormValues) {
    const { username, password } = values;
    dispatch(logUserIn(username, password));
  }

  function showForm(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, formName: 'register' | 'forgotPassword') {
    e.preventDefault();
    props.setCurrentAuthForm(formName);
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <label className='headtext'>sign in with atlas</label>
      <span>
        <label htmlFor='username'>Username</label>
        <Input
          type={inputTypes.text}
          name='username'
          id='username'
          placeholder='johndoe69'
          ref={register}
          errorText={errors.username?.message}
        />
      </span>
      <span>
        <label htmlFor='password'>Password</label>
        <Input
          type={inputTypes.password}
          name='password'
          id='password'
          placeholder='MyP@$sw0rD'
          ref={register}
          errorText={errors.password?.message}
        />
      </span>

      <div className='registerforgotpass'>
        <a onClick={(e) => showForm(e, 'register')} className='button'>
          Register
        </a>
        <a className='button' onClick={(e) => showForm(e, 'forgotPassword')}>
          Forgot Password
        </a>
      </div>
      <Button className='submit colored huge' displayText='Submit' type='submit' disabled={isLoading} />
    </form>
  );
}
