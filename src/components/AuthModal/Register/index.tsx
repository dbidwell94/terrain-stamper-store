import React, { Dispatch, SetStateAction } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@sc/Button';
import Input, { inputTypes } from '@sc/Input';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { registerUser } from '@state/AuthReducer/AuthActions';
import { IFormNames } from '..';

const formSchema = yup.object().shape({
  username: yup
    .string()
    .required('Username is required')
    .min(4, 'Username is too short')
    .max(30, 'Username is too long'),
  email: yup.string().required('Email is required').email(),
  password: yup.string().required('Password is required').min(6, 'Password too short').max(30, 'Password too long'),
  confirmPassword: yup
    .string()
    .required('Confirm Password is requried')
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});

interface IFormData {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}

interface IRegisterProps {
  setCurrentAuthForm: Dispatch<SetStateAction<IFormNames>>;
}

export default function Register(props: IRegisterProps) {
  const { register, handleSubmit, errors } = useForm<IFormData>({ resolver: yupResolver(formSchema) });
  const { isLoading } = useSelector((state) => state.authReducer);
  const dispatch = useDispatch();

  function onSubmit(data: IFormData) {
    const { email, password, username } = data;
    dispatch(registerUser(username, email, password));
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <span>
        <label htmlFor='username'>Username</label>
        <Input
          type={inputTypes.text}
          name='username'
          id='username'
          placeholder='johndoe69'
          ref={register}
          errorText={errors['username']?.message}
        />
      </span>
      <span>
        <label htmlFor='email'>Email</label>
        <Input
          type={inputTypes.email}
          name='email'
          id='email'
          placeholder='johndoe69@iluvstamper.com'
          ref={register}
          errorText={errors['email']?.message}
        />
      </span>
      <span>
        <label htmlFor='password'>Password</label>
        <Input
          type={inputTypes.password}
          name='password'
          id='password'
          placeholder='MyP@$sw0rD'
          ref={register}
          errorText={errors['password']?.message}
        />
      </span>
      <span>
        <label htmlFor='confirmPassword'>Confirm Password</label>
        <Input
          type={inputTypes.password}
          name='confirmPassword'
          id='conformPassword'
          placeholder='MyP@$sw0rD'
          ref={register}
          errorText={errors['confirmPassword']?.message}
        />
      </span>
      <div className='registerforgotpass'>
        <a onClick={() => props.setCurrentAuthForm('login')} className='button'>
          Login
        </a>
      </div>
      <div>
        <Button displayText='Submit' type='submit' disabled={isLoading} />
      </div>
    </form>
  );
}
