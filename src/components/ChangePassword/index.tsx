import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import Button from '@sc/Button';
import Input, { inputTypes } from '@sc/Input';
import { AuthContainer, ParentAuthContainer } from '../AuthModal';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import api from '@api/index';
import { ApiError } from '@api/apiClient';

interface IChangePasswordProps {
  token: string;
}

interface IFormData {
  password: string;
  confirmPassword: string;
}

const formSchema = yup.object().shape({
  password: yup.string().required('Password is required').min(6, 'Password too short').max(30, 'Password too long'),
  confirmPassword: yup
    .string()
    .required('Confirm Password is requried')
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});

export default function ChangePassword(props: IChangePasswordProps) {
  const { token } = props;
  const [serverError, setServerError] = useState<string | null>(null);
  const [passwordChangeSuccess, setPasswordChangeSuccess] = useState(false);

  const { register, errors, handleSubmit } = useForm<IFormData>({ resolver: yupResolver(formSchema) });

  async function onSubmit(data: IFormData) {
    const { password } = data;

    try {
      await api.userApi.resetPasswordWithEmailToken(token, password);
      setServerError(null);
      setPasswordChangeSuccess(true);
    } catch (err) {
      if (err instanceof ApiError) {
        setServerError(err.message);
        return;
      }
      setServerError(
        'There was an error processing your request. Please send for a new forgot password email and try again.'
      );
    }
  }

  return (
    <ParentAuthContainer>
      <AuthContainer inactiveBackground='grey' background='black' borderRadius={1.5}>
        <div className='authboxhead'>
          Change Password
          <Button displayText='Close' onClick={() => window.location.replace('/')} />
        </div>
        <div className='content'>
          {serverError && (
            <span className='server-error'>
              <p>{serverError}</p>
            </span>
          )}
          {passwordChangeSuccess && (
            <span className='server-success'>
              <p>Password Change Successful</p>
            </span>
          )}

          <form onSubmit={handleSubmit(onSubmit)}>
            <span>
              <label htmlFor='password'>Password</label>
              <Input
                type={inputTypes.password}
                id='password'
                placeholder='P@s$w0Rd'
                name='password'
                errorText={errors.password?.message}
                ref={register}
              />
            </span>
            <span>
              <label htmlFor='confirmPassword'>Confirm Password</label>
              <Input
                type={inputTypes.password}
                id='confirmPassword'
                placeholder='P@s$w0Rd'
                name='confirmPassword'
                errorText={errors.confirmPassword?.message}
                ref={register}
              />
            </span>
            <Button displayText='Submit' type='submit' />
          </form>
        </div>
      </AuthContainer>
    </ParentAuthContainer>
  );
}
