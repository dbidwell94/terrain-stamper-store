import React, { Suspense, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { getStamps, getUserPurchasedStamps, setCurrentlyViewedStamp } from '@state/StampReducer/StampActions';
const StampView = React.lazy(() => import('./StampView'));
import Loading from '@sc/Loading';
import { IStampView } from 'src/api/stampApi';
import qs from 'query-string';
import GridContainer from '@sc/GridContainer';
import { getPackages, setCurrentlyViewedPackage } from '@state/PackageReducer/PackageActions';
import { IContentFilter } from '../App';
import { arrayEquals } from '@src/utils';

const ContentFeedContainer = styled.div`
  padding: ${({ theme }) => `${theme.global.padding}rem`};
  display: flex;
  flex-direction: column;
  width: 100%;
  max-height: 100%;
  grid-column: 2 / -1;
  grid-row: 2 / -1;
  overflow-x: hidden;
  overflow-y: auto;
  background: linear-gradient(to bottom, #00000026, transparent 15px);
`;

interface IContentFeedProps {
  className?: string;
  searchbarPills?: string[];
  filter: IContentFilter;
}

export default function ContentFeed({ className, searchbarPills, filter }: IContentFeedProps) {
  const dispatch = useDispatch();

  const { stampsLoadingCalls, stamps, userPurchasedStamps, currentFilteredCategories } = useSelector(
    (state) => state.stampReducer
  );
  const { packagesLoadingCount } = useSelector((state) => state.packageReducer);
  const { currentUser } = useSelector((state) => state.authReducer);

  const params = useMemo(() => {
    return qs.parse(window.location.search);
  }, []);
  useEffect(() => {
    dispatch(getPackages());
    dispatch(getUserPurchasedStamps());
    dispatch(getStamps());
  }, [currentUser]);

  // Logic to filter and search though the stamps
  const stampsToView = useMemo<IStampView[]>(() => {
    let filteredStamps: IStampView[];
    if (currentFilteredCategories.length < 1) {
      filteredStamps = stamps;
    } else {
      filteredStamps = stamps.filter((s) =>
        arrayEquals(s.categories.slice(0, currentFilteredCategories.length), currentFilteredCategories)
      );
    }

    const filteredTypes = filteredStamps.filter((s) => {
      switch (filter.type) {
        case 'all types':
          return true;
        default:
          return s.stampType.toLocaleLowerCase().includes(filter.type);
      }
    });

    const resolutionFilter = filteredTypes.filter((s) => {
      switch (filter.resolutions) {
        case 'all resolutions':
          return true;

        default: {
          const stampResolutions = s.files.map((f) => f.resolution);
          return stampResolutions.includes(filter.resolutions);
        }
      }
    });

    const statusFilter = resolutionFilter.filter((s) => {
      switch (filter.status) {
        case 'new': {
          const currentDate = new Date(Date.now());
          currentDate.setMonth(currentDate.getMonth() - 1);
          return Date.parse(s.releaseDate) > currentDate.getTime();
        }

        case 'purchased': {
          return Boolean(userPurchasedStamps.find((purchased) => purchased.id === s.id));
        }

        case 'all stamps':
          return true;

        default:
          return true;
      }
    });

    return statusFilter.filter((stamp) => {
      if (!searchbarPills || searchbarPills.length < 1) {
        return true;
      } else {
        return searchbarPills
          .map((rule) => {
            const foundName = stamp.name.toLocaleLowerCase().includes(rule);
            const foundType = stamp.stampType.toLocaleLowerCase().includes(rule);
            const foundPrice = stamp.price.toString().toLocaleLowerCase().includes(rule);
            const foundPackageName = stamp.package?.name.toLocaleLowerCase().includes(rule);
            const foundPackageDescription = stamp.package?.description.toLocaleLowerCase().includes(rule);
            return foundName || foundType || foundPrice || foundPackageName || foundPackageDescription;
          })
          .reduce<boolean>((val, currentVal) => {
            if (!val) return val;
            return currentVal || false;
          }, true);
      }
    });
  }, [searchbarPills, stamps, filter, currentFilteredCategories]);

  useEffect(() => {
    if (stamps.length === 0 || stampsLoadingCalls > 0) {
      return;
    }
    if (params['stampPurchased']) {
      dispatch(setCurrentlyViewedStamp(Number(params.stampPurchased)));
    } else if (params['packagePurchased']) {
      dispatch(setCurrentlyViewedPackage(Number(params.packagePurchased)));
    }
  }, [stampsLoadingCalls, stamps]);

  return (
    <ContentFeedContainer>
      {stampsLoadingCalls > 0 ? (
        <Loading />
      ) : (
        <>
          <GridContainer
            className={`${className ? className : ''} ${
              stampsLoadingCalls > 0 || packagesLoadingCount > 0 ? 'loading' : ''
            }`}
            noPaddingHorizontal
            noScroll
          >
            {stampsToView.map((stamp, index) => {
              return (
                <Suspense fallback={<Loading />} key={`stamp-${index}`}>
                  <StampView stamp={stamp} />
                </Suspense>
              );
            })}
          </GridContainer>
        </>
      )}
    </ContentFeedContainer>
  );
}
