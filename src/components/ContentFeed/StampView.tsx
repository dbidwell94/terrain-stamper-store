import React, { useState } from 'react';
import { IStampView } from 'src/api/stampApi';
import styled from 'styled-components';
import LoadingSvg from 'src/sharedComponents/Loading';
import { useDispatch } from 'react-redux';
import { setCurrentlyViewedStamp } from 'src/state/StampReducer/StampActions';
import { serverUrl } from 'src/api';

interface IStampContainerOptions {
  isUnreleased?: boolean;
  sizeOverride?: number;
}

const StampContainer = styled.div<IStampContainerOptions>`
  z-index: 0;
  position: relative;
  width: ${({ theme, sizeOverride }) => sizeOverride || theme.global.stampViewSize}rem;
  height: ${({ theme, sizeOverride }) => sizeOverride || theme.global.stampViewSize}rem;
  background: ${({ theme }) => theme.global.colors.stampBackground};
  margin: ${({ theme }) => theme.global.padding / 6}rem;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  text-align: center;
  flex-direction: column;
  border-radius: 10px;
  img {
    transition: 1s ease-in-out all;
    position: absolute;
    top: 0;
    right: 0;
    width: 20rem;
    height: 20rem;
    object-fit: cover;
    border-radius: 50%;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    filter: opacity(0);
    &.active {
      filter: opacity(100);
    }
    &.hidden {
      filter: opacity(0);
    }
  }

  div.tag {
    position: absolute;
    border-radius: 0.5rem;
    background-color: ${({ isUnreleased }) => (isUnreleased ? 'orange' : '#0087ff')};
    left: 1rem;
    top: 1rem;
    padding: 0.5rem 1rem;
    color: white;
    p {
      font-size: 11pt;
    }
  }

  div.overlay {
    border-radius: 1rem;
    border: 0.2rem solid gray;
    display: grid;
    transition: 0.125s ease-in-out all;
    width: 100%;
    height: 100%;
    position: absolute;
    color: white;
    opacity: 0;
    grid-template-columns: repeat(4, 1fr);
    grid-template-rows: repeat(4, 1fr);
    box-shadow: 0px 0.5rem 1.5rem #00000050;
    &:hover {
      opacity: 100%;
    }
    div {
      &.stampname {
        position: absolute;
        bottom: 0;
        padding: 1rem;
      }

      h4,
      p {
        font-weight: bold;
        font-size: 11pt !important;
      }
    }
  }
`;

interface IStampViewProps {
  stamp: IStampView;
  sizeOverride?: number;
}

export default function StampView(props: IStampViewProps) {
  const dispatch = useDispatch();

  const { stamp, sizeOverride } = props;

  const [pictureLoaded, setPictureLoaded] = useState(false);

  if (!stamp) {
    return <LoadingSvg />;
  }

  return (
    <StampContainer
      onClick={() => dispatch(setCurrentlyViewedStamp(stamp.id))}
      isUnreleased={!stamp.isReleased}
      sizeOverride={sizeOverride}
    >
      <img
        src={`${serverUrl}/api/pictures/stamp/${stamp.id}/index`}
        className={`${pictureLoaded ? 'active' : 'hidden'}`}
        onLoad={() => setPictureLoaded(true)}
        alt={`Picture of stamp ${stamp.name}`}
      />
      {!pictureLoaded && <LoadingSvg className='active' />}
      <div className='tag'>
        <p>{`${stamp.stampType}`}</p>
      </div>
      <div className='overlay'>
        <div className='stampname'>
          <h4>{stamp.name}</h4>
        </div>
      </div>
    </StampContainer>
  );
}
