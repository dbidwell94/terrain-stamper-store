import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { IContentFilter } from '@components/App';
import Select from '@sc/Select';
import styled from 'styled-components';

const FilterContainer = styled.div`
  padding: 0rem ${({ theme }) => `${theme.global.padding}`}rem;
  grid-row: 2 / 3;
  display: flex;
  justify-content: flex-start;
  align-items: center;

  .selectcontainer {
    border-right: 1px solid #575757;
    padding-right: 1rem;
    margin-right: 1rem;

    select {
      background: transparent;
      padding: 0;
    }
  }
`;

const stampStates = ['all stamps', 'new', 'purchased'];

const types: IContentFilter['type'][] = ['all types', 'road', 'terrain'];

interface IFilterProps {
  filters: IContentFilter;
  setFilters: React.Dispatch<React.SetStateAction<IContentFilter>>;
}

export default function Filter(props: IFilterProps) {
  const { setFilters, filters } = props;

  const { stamps } = useSelector((state) => state.stampReducer);

  const supportedResolutions = useMemo<string[]>(() => {
    const resolutions: Set<string> = stamps.reduce<Set<string>>((set, currentStamp) => {
      for (const f of currentStamp.files) {
        set = set.add(f.resolution);
      }
      return set;
    }, new Set());
    return ['all resolutions', ...resolutions];
  }, [stamps]);

  return (
    <FilterContainer>
      <div className='selectcontainer'>
        <Select
          options={types.map((type, index) => {
            return {
              key: `stamp-type-${type}-${index}`,
              value: type,
            };
          })}
          value={filters.type}
          onChange={(e) => {
            const { value } = e.target;

            setFilters({ ...filters, type: value as IContentFilter['type'] });
          }}
          name='stamp-type-filter'
          id='stamp-type-filter'
        />
      </div>
      <div className='selectcontainer'>
        <Select
          options={supportedResolutions.map((resolution, index) => {
            return {
              key: `stamp-resolution-${resolution}-${index}`,
              value: resolution,
            };
          })}
          value={filters.resolutions}
          onChange={(e) => {
            const { value } = e.target;
            setFilters({ ...filters, resolutions: value as IContentFilter['resolutions'] });
          }}
          name='stamp-resolution-filter'
          id='stamp-resolution-filter'
        />
      </div>
      <div className='selectcontainer'>
        <Select
          options={stampStates.map((state, index) => {
            return {
              key: `stamp-resolution-${state}-${index}`,
              value: state,
            };
          })}
          value={filters.status}
          onChange={(e) => {
            const { value } = e.target;
            setFilters({ ...filters, status: value as IContentFilter['status'] });
          }}
          name='stamp-state-filter'
          id='stamp-state-filter'
        />
      </div>
    </FilterContainer>
  );
}
