import React from 'react';
import { useDispatch } from 'react-redux';
import ButtonLink from '@sc/ButtonLink';
import { logUserOut } from '@state/AuthReducer/AuthActions';
import styled from 'styled-components';

const DropdownContainer = styled.div`
  z-index: 1;
  position: absolute;
  top: calc(100% - ${({ theme }) => theme.global.padding / 2}rem);
  background: transparent;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 0;
  padding-top: ${({ theme }) => theme.global.padding / 2}rem;
  margin: 0;

  .buttonlink:last-child {
    button {
      border-radius: 0 0 ${({ theme }) => `${theme.global.padding / 1.5}rem ${theme.global.padding / 1.5}rem`};
    }
  }
`;

type IDropdownProps = Omit<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, 'ref'>;

export default function ProfileOptionsDropdown(props: IDropdownProps) {
  const { ...divProps } = props;

  const dispatch = useDispatch();

  return (
    <DropdownContainer {...divProps}>
      <ButtonLink className='buttonlink' to={'/profile'} displayText='Profile' />
      <ButtonLink className='buttonlink' to={'/'} onClick={() => dispatch(logUserOut())} displayText='Logout' />
    </DropdownContainer>
  );
}
