import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Button from '@sc/Button';
import styled from 'styled-components';
import ProfileOptionsDropdown from './ProfileOptionsDropdown';

const ProfileOptionsMenuContainer = styled.div`
  position: relative;

  .halfsquared {
    border-radius: ${({ theme }) => `${theme.global.padding / 1.5}rem`}
      ${({ theme }) => `${theme.global.padding / 1.5}rem`} 0 0;
  }
  button {
    z-index: 10;
  }
`;

export default function ProfileOptionsMenu() {
  const { currentUser } = useSelector((state) => state.authReducer);

  const [dropdownShowing, setDropdownShowing] = useState(false);

  return (
    <ProfileOptionsMenuContainer>
      <Button
        displayText={currentUser?.username}
        className={dropdownShowing ? 'halfsquared dark' : 'dark'}
        onMouseEnter={() => setDropdownShowing(true)}
        onMouseLeave={() => setDropdownShowing(false)}
      />
      {dropdownShowing && (
        <ProfileOptionsDropdown
          className='dropdown'
          onMouseLeave={() => setDropdownShowing(false)}
          onMouseEnter={() => setDropdownShowing(true)}
        />
      )}
    </ProfileOptionsMenuContainer>
  );
}
