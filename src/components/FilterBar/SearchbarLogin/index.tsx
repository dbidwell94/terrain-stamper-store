import React from 'react';
import styled from 'styled-components';
import Button from '@sc/Button';
import Searchbar from './Searchbar';
import { useDispatch, useSelector } from 'react-redux';
import { toggleAuthModal } from '@state/AuthReducer/AuthActions';
import ProfileOptionsMenu from './ProfileOptionsMenu';

const SearchbarLoginContainer = styled.div`
  padding: 0rem ${({ theme }) => `${theme.global.padding / 2}`}rem;
  grid-row: 1 / 2;
  grid-column: 1 / 1;
  display: flex;
  align-items: center;
`;

interface ISearchbarLoginProps {
  className?: string;
  setSearchbarText: React.Dispatch<React.SetStateAction<string[]>>;
}

export default function SearchbarLogin({ className, setSearchbarText }: ISearchbarLoginProps) {
  const dispatch = useDispatch();
  const { authModalOpen, token } = useSelector((state) => state.authReducer);

  return (
    <SearchbarLoginContainer className={className}>
      <Searchbar setSearchbarText={setSearchbarText} />
      {token ? (
        <ProfileOptionsMenu />
      ) : (
        <Button className='colored' displayText='Login' onClick={() => dispatch(toggleAuthModal(!authModalOpen))} />
      )}
    </SearchbarLoginContainer>
  );
}
