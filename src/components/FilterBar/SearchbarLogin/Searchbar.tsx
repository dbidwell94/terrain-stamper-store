import React, { useEffect, useState } from 'react';
import InputWithPills from '@sc/InputWithPills';
import styled from 'styled-components';

const SearchbarContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  max-width: 100%;
  margin-right: 1.5rem;
`;

interface ISearchbarProps {
  setSearchbarText: React.Dispatch<React.SetStateAction<string[]>>;
}

export default function Searchbar(props: ISearchbarProps) {
  const [filterPills, setFilterPills] = useState<string[]>([]);

  function onChange(strings: string[]) {
    setFilterPills(strings);
  }

  useEffect(() => {
    props.setSearchbarText(filterPills);
  }, [filterPills]);

  return (
    <SearchbarContainer>
      <label htmlFor='searchbar' hidden>
        Search
      </label>
      <InputWithPills
        pills={filterPills}
        onChange={onChange}
        placeholder='Input search terms seperated by space or a commas'
        maxPills={5}
        rounded
      />
    </SearchbarContainer>
  );
}
