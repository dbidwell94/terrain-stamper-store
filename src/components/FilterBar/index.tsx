import React from 'react';
import styled from 'styled-components';
import { IContentFilter } from '@components/App';
import Filter from './Filter';
import SearchbarLogin from './SearchbarLogin';

const FilterBarContainer = styled.div`
  grid-column: 2 / -1;
  grid-row: 1 / 1;
  display: grid;
  grid-template-rows: repeat(2, 1fr);
  padding-top: ${({ theme }) => theme.global.padding / 2}rem;
  .filter {
    grid-row: 2 / 1;
  }
`;

interface IFilterBarProps {
  className?: string;
  setSearchbarText: React.Dispatch<React.SetStateAction<string[]>>;
  filters: IContentFilter;
  setFilters: React.Dispatch<React.SetStateAction<IContentFilter>>;
}

export default function FilterBar({ className, setSearchbarText, filters, setFilters }: IFilterBarProps) {
  return (
    <FilterBarContainer className={className}>
      <SearchbarLogin setSearchbarText={setSearchbarText} />
      <Filter filters={filters} setFilters={setFilters} />
    </FilterBarContainer>
  );
}
