import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IStampView } from '@api/stampApi';
import { IPackageEdit } from '@api/packageApi';
import GridContainer from '@sc/GridContainer';
import ModalContainer from '@sc/ModalContainer';
import { getStamps } from '@state/StampReducer/StampActions';
import styled from 'styled-components';
import Folder from './Folder';
import NoPackageStamp from './NoPackageStamp';
import { v4 as uuid } from 'uuid';
import Button from '@sc/Button';
import api from '@api/index';
import Input, { inputTypes } from '@sc/Input';

const PackageSaveModal = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  max-width: 50rem;
  max-height: 50rem;
  width: 50%;
  height: 50%;
  background: black;
  z-index: 2;
  border-radius: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1.5rem;
  hr {
    width: 100%;
    margin-bottom: 1rem;
    color: white;
    background: white;
  }
  textarea {
    width: 100%;
    height: 85%;
    resize: none;
    outline: none;
    border: none;
    margin-bottom: 1rem;
    color: black;
  }
`;

export default function EditPackages() {
  const { stamps } = useSelector((state) => state.stampReducer);

  const [packagelessStamps, setPackagelessStamps] = useState<IStampView[]>([]);
  const [packages, setPackages] = useState<IPackageEdit[]>([]);

  const [currentlyDraggingStamp, setCurrentlyDraggingStamp] = useState<IStampView | null>(null);
  const [packageToSave, setPackageToSave] = useState<IPackageEdit | null>(null);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getStamps());
    api.packageApi
      .getAllPackages()
      .then((p) => {
        const packageEdits: IPackageEdit[] = p.map((pkg) => {
          return { ...pkg, saved: true };
        });
        setPackages([...packages, ...packageEdits]);
      })
      .catch(() => setPackages([]));
  }, []);

  useEffect(() => {
    if (!stamps) return;
    setPackagelessStamps(stamps.filter((stamp) => !stamp.package));
  }, [stamps]);

  function handleDrop(draggedStamp: IStampView, currentStamp: IStampView) {
    console.log('dropped');
    const newPkg: IPackageEdit = {
      description: '',
      id: uuid(),
      name: `New Package ${packages.length + 1}`,
      stamps: [draggedStamp, currentStamp],
      saved: false,
      discountMultiplier: 0.0,
    };
    setPackagelessStamps(
      packagelessStamps.filter((stamp) => stamp.id !== draggedStamp.id && stamp.id !== currentStamp.id)
    );
    setPackages([newPkg, ...packages]);
  }

  function handlePackageSaveButtonClick(pkg: IPackageEdit) {
    setPackageToSave(pkg);
  }

  function handleFolderDrop(pkg: IPackageEdit, stamp: IStampView) {
    setPackagelessStamps(packagelessStamps.filter((pstamp) => pstamp.id !== stamp.id));
    setPackages(
      packages.map((p) => {
        if (p.id === pkg.id) {
          return { ...p, stamps: [...p.stamps, stamp], saved: false };
        }
        return p;
      })
    );
  }

  async function deletePackage(pkg: IPackageEdit) {
    if (typeof pkg.id === 'number') {
      await api.packageApi.deletePackageById(pkg.id);
    }
  }

  async function removeStampFromPackage(pkg: IPackageEdit, stamp: IStampView) {
    setPackagelessStamps([...packagelessStamps, stamp]);

    const pkgs: IPackageEdit[] = [];
    for (const p of packages) {
      if (p.id === pkg.id) {
        const newPkg: IPackageEdit = { ...p, stamps: p.stamps.filter((s) => s.id !== stamp.id), saved: false };
        if (newPkg.stamps.length > 0) {
          pkgs.push(newPkg);
        } else {
          await deletePackage(p);
        }
      } else {
        pkgs.push(p);
      }
    }
    setPackages(pkgs);
  }

  function editPackageName(pkg: IPackageEdit, name: string) {
    setPackages(
      packages.map((p) => {
        if (p.id !== pkg.id) return p;
        return { ...p, name, saved: false };
      })
    );
  }

  async function submitPackageToServer(evt: React.MouseEvent<HTMLButtonElement>) {
    evt.preventDefault();
    if (!packageToSave) return;
    if (typeof packageToSave.id === 'string') {
      const serverId = await api.packageApi.submitNewPackage(packageToSave);
      const newPackage: IPackageEdit = { ...packageToSave, id: serverId, saved: true };
      setPackages(
        packages.map((p) => {
          if (p.id === packageToSave.id) {
            return newPackage;
          }
          return p;
        })
      );
      setPackageToSave(null);
    } else {
      await api.packageApi.modifyPackage(packageToSave);
      setPackages(
        packages.map((p) => {
          if (p.id === packageToSave.id) {
            return { ...packageToSave, saved: true };
          }
          return p;
        })
      );
      setPackageToSave(null);
    }
  }

  return (
    <>
      {packageToSave && (
        <ModalContainer onClick={() => setPackageToSave(null)}>
          <PackageSaveModal>
            <h1>{packageToSave.name}</h1>
            <hr />
            <textarea
              placeholder='Description...'
              onChange={(evt) => {
                const { value } = evt.target;
                setPackageToSave({ ...packageToSave, description: value });
              }}
              value={packageToSave.description}
            />
            <label>Discount Percentage</label>
            <Input
              type={inputTypes.text}
              value={`${packageToSave.discountMultiplier * 100}%`}
              onChange={(evt) => {
                const { value } = evt.target;
                const numericalValue = Number(value.replace('%', '')) / 100;
                if (!Number.isNaN(numericalValue)) {
                  setPackageToSave({ ...packageToSave, discountMultiplier: numericalValue });
                }
              }}
            />
            <Button displayText='Submit' onClick={submitPackageToServer} />
          </PackageSaveModal>
        </ModalContainer>
      )}
      <GridContainer>
        {packages.map((pkg) => (
          <Folder
            stamps={pkg.stamps}
            key={`new-pkg-${pkg.id}`}
            onDrop={(stamp) => handleFolderDrop(pkg, stamp)}
            currentlyDraggingStamp={currentlyDraggingStamp}
            removeStampFromPackage={(stamp) => removeStampFromPackage(pkg, stamp)}
            name={pkg.name}
            editPackageName={(newName) => editPackageName(pkg, newName)}
            handleSaveButtonClick={() => handlePackageSaveButtonClick(pkg)}
            saved={pkg.saved}
          />
        ))}
        {packagelessStamps.map((stamp) => (
          <NoPackageStamp
            stamp={stamp}
            key={`packageless-stamp-id-${stamp.id}`}
            onDropWithStamp={handleDrop}
            setCurrentDraggingStamp={setCurrentlyDraggingStamp}
            currentDraggingStamp={currentlyDraggingStamp}
          />
        ))}
      </GridContainer>
    </>
  );
}
