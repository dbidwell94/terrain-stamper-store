import React, { useState } from 'react';
import { serverUrl } from '@api/index';
import { IStampView } from '@api/stampApi';
import Button from '@sc/Button';
import Input, { inputTypes } from '@sc/Input';
import styled from 'styled-components';

const FolderContainer = styled.div`
  display: flex;
  width: ${({ theme }) => theme.global.stampViewSize}rem;
  height: ${({ theme }) => theme.global.stampViewSize}rem;
  margin: ${({ theme }) => theme.global.padding / 6}rem;
  border-radius: 1rem;
  border: thin solid red;
  background: ${({ theme }) => theme.global.colors.stampBackground};
  position: relative;

  &.saved {
    border: thin solid green;
  }

  div.package-settings {
    position: absolute;
    top: 100%;
    left: 50%;
    transform: translate(-50%, -100%);
    display: none;
    z-index: 1;
  }

  &:hover {
    div.package-settings {
      display: flex;
      filter: opacity(0.25);
      transition: all 0.125s ease-in-out;
      &:hover {
        filter: opacity(1);
      }
    }
  }

  &.over {
    border: thin solid grey;
  }

  div.content {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;

    overflow-x: hidden;
    overflow-y: auto;

    // Scrollbar style (NONE!)
    -ms-overflow-style: none;
    scrollbar-width: none;
    ::-webkit-scrollbar {
      display: none;
    }

    div.package-title {
      width: 100%;
      height: auto;
      display: flex;
      justify-content: center;
      align-items: center;
      background: grey;
      border-top-left-radius: 1rem;
      border-top-right-radius: 1rem;
      cursor: pointer;
      .name-change {
        width: 100%;
        border-top-right-radius: 1rem;
        border-top-left-radius: 1rem;
      }
    }
    div.grid {
      width: 100%;
      max-height: 100%;
      display: grid;
      grid-gap: 0.5rem;
      grid-template-columns: repeat(auto-fill, minmax(10rem, 1fr));
      grid-template-rows: repeat(auto-fill, minmax(10rem, 1fr));
    }
  }
`;

const StampFolderDisplay = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  position: relative;
  p {
    font-size: 1rem;
    text-align: center;
  }

  div.remove {
    width: 1.5rem;
    height: 1.5rem;
    border-radius: 1.5rem;
    background: red;
    p {
      color: black;
      cursor: default;
    }
    position: absolute;
    top: 0%;
    right: 0%;
    transform: translate(0%, 100%);
    display: none;
  }

  &:hover {
    div.remove {
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;

interface IFolderProps {
  stamps: IStampView[];
  currentlyDraggingStamp: IStampView | null;
  removeStampFromPackage: (stamp: IStampView) => void;
  onDrop?: (stamp: IStampView) => void;
  name: string;
  saved?: boolean;
  editPackageName: (newName: string) => void;
  handleSaveButtonClick: () => void;
}

export default function Folder(props: IFolderProps) {
  const {
    stamps,
    onDrop,
    currentlyDraggingStamp,
    removeStampFromPackage,
    name,
    editPackageName,
    saved,
    handleSaveButtonClick,
  } = props;
  const [isDraggedOver, setIsDraggedOver] = useState(false);
  const [tempNameValue, setTempNameValue] = useState(name);

  const [isEditingPackageName, setIsEditingPackageName] = useState(false);

  function handleDragEnter(_: React.DragEvent<HTMLDivElement>) {
    if (currentlyDraggingStamp) {
      if (!currentlyDraggingStamp.id) return;
      setIsDraggedOver(true);
    }
  }

  function handleDragExit(_: React.DragEvent<HTMLDivElement>) {
    if (currentlyDraggingStamp) {
      if (!currentlyDraggingStamp.id) return;
      setIsDraggedOver(false);
    }
  }

  function handleDrop(_: React.DragEvent<HTMLDivElement>) {
    if (currentlyDraggingStamp) {
      if (!currentlyDraggingStamp.id) return;
      setIsDraggedOver(false);
      onDrop && onDrop(currentlyDraggingStamp);
    }
  }

  function startEditPackageName(evt: React.MouseEvent<HTMLDivElement>) {
    setIsEditingPackageName(true);
    evt.preventDefault();
  }

  function endEditPackageName(evt: React.FocusEvent<HTMLDivElement>) {
    setIsEditingPackageName(false);
    editPackageName(tempNameValue);
    evt.preventDefault();
  }

  return (
    <FolderContainer
      onDragOver={(evt) => evt.preventDefault()}
      onDragEnter={handleDragEnter}
      onDragLeave={handleDragExit}
      onDrop={handleDrop}
      className={`${isDraggedOver ? 'over' : ''} ${saved ? 'saved' : ''}`}
    >
      <div className='package-settings'>
        <Button displayText='Save...' onClick={handleSaveButtonClick} />
      </div>
      <div className='content'>
        <div className='package-title' onClick={startEditPackageName} onBlur={endEditPackageName}>
          {isEditingPackageName ? (
            <Input
              type={inputTypes.text}
              value={tempNameValue}
              className='name-change'
              onChange={(evt) => {
                evt.preventDefault();
                const { value } = evt.target;
                setTempNameValue(value);
              }}
            />
          ) : (
            <h4>{tempNameValue}</h4>
          )}
        </div>
        <div className='grid'>
          {stamps.map((stamp) => {
            return (
              <StampFolderDisplay key={`folder-stamp-${stamp.id}`}>
                <div className='remove' onClick={() => removeStampFromPackage(stamp)}>
                  <p>X</p>
                </div>
                <img src={`${serverUrl}/api/pictures/stamp/${stamp.id}/index`} alt={stamp.name} />
                <p>{stamp.name}</p>
              </StampFolderDisplay>
            );
          })}
        </div>
      </div>
    </FolderContainer>
  );
}
