import React, { useState } from 'react';
import { serverUrl } from 'src/api';
import { IStampView } from 'src/api/stampApi';
import styled from 'styled-components';

const StampContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ theme }) => theme.global.stampViewSize}rem;
  height: ${({ theme }) => theme.global.stampViewSize}rem;
  margin: ${({ theme }) => theme.global.padding / 6}rem;

  position: relative;
  border-radius: 1rem;
  background: ${({ theme }) => theme.global.colors.stampBackground};

  div.details {
    position: absolute;
    display: grid;
    grid-template-rows: repeat(10, 1fr);
    width: 100%;
    height: 100%;
    border-radius: 1rem;
    div.name {
      grid-row: 9 / 11;
      margin: auto;
    }
  }

  &.dragging {
    filter: opacity(0.2);
  }
  &.over {
    border: thin solid grey;
  }
`;

interface INoPackageStampProps {
  stamp: IStampView;
  currentDraggingStamp: IStampView | null;
  setCurrentDraggingStamp: React.Dispatch<React.SetStateAction<IStampView | null>>;
  onDragOverWithStamp?: (draggedStamp: IStampView, currentStamp: IStampView) => void;
  onDragExitWithStamp?: (draggedStamp: IStampView, currentStamp: IStampView) => void;
  onDropWithStamp?: (draggedStamp: IStampView, currentStamp: IStampView) => void;
}

export default function NoPackageStamp(props: INoPackageStampProps) {
  const {
    stamp,
    onDragOverWithStamp,
    onDragExitWithStamp,
    onDropWithStamp,
    currentDraggingStamp,
    setCurrentDraggingStamp,
  } = props;

  const [isDragging, setIsDragging] = useState(false);
  const [isDraggedOver, setIsDraggedOver] = useState(false);

  const dragImage = React.useRef<HTMLImageElement | null>(null);

  function handleDragStart(evt: React.DragEvent<HTMLDivElement>) {
    setIsDragging(true);
    setCurrentDraggingStamp(stamp);
    evt.dataTransfer.effectAllowed = 'move';
    evt.dataTransfer.dropEffect = 'move';
    if (!dragImage.current) return;
    const offsetX = dragImage.current.width / 2;
    const offsetY = dragImage.current.height / 2;
    evt.dataTransfer.setDragImage(dragImage.current, offsetX, offsetY);
  }

  function handleDragEnd(_: React.DragEvent<HTMLDivElement>) {
    setIsDragging(false);
    setCurrentDraggingStamp(null);
  }

  function handleDragEnter(evt: React.DragEvent<HTMLDivElement>) {
    if (currentDraggingStamp) {
      if (currentDraggingStamp.id !== stamp.id) {
        setIsDraggedOver(true);
        onDragOverWithStamp && onDragOverWithStamp(currentDraggingStamp, stamp);
      }
    }
    evt.preventDefault();
  }

  function handleDragExit(evt: React.DragEvent<HTMLDivElement>) {
    if (currentDraggingStamp) {
      if (currentDraggingStamp.id !== stamp.id) {
        setIsDraggedOver(false);
        onDragExitWithStamp && onDragExitWithStamp(currentDraggingStamp, stamp);
      }
    }
    evt.preventDefault();
  }

  function handleDrop(evt: React.DragEvent<HTMLDivElement>) {
    if (currentDraggingStamp) {
      if (!currentDraggingStamp.id) return;
      if (currentDraggingStamp.id !== stamp.id) {
        setIsDraggedOver(false);
        onDropWithStamp && onDropWithStamp(currentDraggingStamp, stamp);
      }
    }
    evt.preventDefault();
  }

  return (
    <StampContainer
      draggable
      onDragOver={(evt) => evt.preventDefault()}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragEnter={handleDragEnter}
      onDragLeave={handleDragExit}
      onDrop={handleDrop}
      className={`${isDraggedOver ? 'over' : ''} ${isDragging ? 'dragging' : ''}`}
    >
      <img src={`${serverUrl}/api/pictures/stamp/${stamp.id}/index`} ref={dragImage} alt={stamp.name} />
      <div className='details'>
        <div className='name'>
          <p>{stamp.name}</p>
        </div>
      </div>
    </StampContainer>
  );
}
