import React, { useEffect, useMemo, useState } from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import api from '@api/index';
import { IStampView } from '@api/stampApi';
import StampDetailView from './StampDetailView';

export default function MyStamps() {
  const [myStamps, setMyStamps] = useState<IStampView[]>([]);

  useEffect(() => {
    api.stampApi
      .getMyUploadedStamps()
      .then((res) => {
        setMyStamps(res);
      })
      .catch(console.error);
  }, []);

  const { stampId } = useParams<{ stampId?: string }>();

  const currentStamp = useMemo<IStampView | null>(() => {
    if (!stampId || myStamps.length < 1) return null;
    const toReturn = myStamps.find((stamp) => stamp.id === Number(stampId));
    return toReturn || null;
  }, [stampId, myStamps]);

  return (
    <>
      {currentStamp ? (
        <StampDetailView stamp={currentStamp} />
      ) : (
        myStamps.map((stamp) => {
          return (
            <Link to={`/profile/mystamps/${stamp.id}`} key={`stamp-link-${stamp.name}-${stamp.id}`}>
              {stamp.name}
            </Link>
          );
        })
      )}
    </>
  );
}
