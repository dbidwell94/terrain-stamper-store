import React, { useEffect, useState } from 'react';
import { IStampView, IUploadFileArgs } from 'src/api/stampApi';
import Input, { inputTypes } from 'src/sharedComponents/Input';
import Button from 'src/sharedComponents/Button';
import Select from 'src/sharedComponents/Select';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { getStamps, refreshCategories, setCurrentlyViewedStamp } from 'src/state/StampReducer/StampActions';
import api from 'src/api';
import InputWithPills from 'src/sharedComponents/InputWithPills';
import { ApiError } from 'src/api/apiClient';
import { Snackbar as MaterialSnackbar } from '@mui/material';

const StampDetailContainer = styled.section<{ uploadProgress: number }>`
  width: 100%;
  height: 100%;
  background: black;
  padding: ${({ theme }) => theme.global.padding}rem;
  overflow: auto;
  &.error {
    border: thin solid red;
  }
  .title {
    color: white;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 20pt;
    padding: ${({ theme }) => theme.global.padding}rem;
  }

  form {
    width: 100%;
    padding: ${({ theme }) => theme.global.padding}rem;

    .dropfield {
      background: #131313;
      margin-bottom: ${({ theme }) => theme.global.padding * 2}rem;

      border: 0.2rem solid transparent;
      position: relative;
      height: 20rem;
      transition: 0.125s ease-in-out all;

      .dropfieldcenter {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translateX(-50%) translateY(-50%);
        color: #3f3f3f;
        text-transform: uppercase;
        font-weight: bold;
        font-size: 36pt;
      }

      &::after {
        position: absolute;
        top: 50%;
        left: 0%;
        transform: translateX(0%) translateY(-50%);
        content: '';
        width: ${({ uploadProgress }) => uploadProgress}%;
        height: 100%;
        border-radius: ${({ theme }) => theme.global.padding * 2}rem;
        background: lightblue;
        opacity: 0.3;
      }
    }

    .dropfield:hover {
      border: 0.2rem solid grey;
    }

    .horizontal {
      grid-gap: ${({ theme }) => theme.global.padding}rem;
      display: flex;
      flex-direction: row;
      .date-pick {
        position: relative;
        input[type~='date'] {
          position: absolute;
          top: 0;
          color: black;
          transform: translate(0rem, -100%);
        }
      }
    }

    .formlabel {
      width: 100%;
      display: block;
      margin-bottom: ${({ theme }) => theme.global.padding * 2}rem;
      pointer-events: none;

      p {
        text-transform: uppercase;
        font-weight: bold;
        color: #474747;
        font-size: 12pt;
        padding: ${({ theme }) => theme.global.padding}rem;
        padding-left: 0;
      }

      select {
        width: 100%;
        height: 50px;
        border: 2px solid transparent;
      }

      select:hover {
        border: 2px solid gray;
      }

      select:focus {
        border: 2px solid #0087ff;
      }
    }
  }
`;

const Snackbar = styled(MaterialSnackbar)`
  div {
    font-size: 1.25rem;
  }
`;

interface IStampDetailView {
  stamp: IStampView | null;
}

interface IFormValues {
  stampFile: File | null;
  price: number;
  stampName: string;
  stampType: string;
  categories: string[];
  releaseDate: Date | null;
}

const supportedPrices = [5, 10, 20];

const stampTypes = ['Terrain', 'Road'];

export default function StampDetailView(props: IStampDetailView) {
  const { stamp } = props;
  const { stamps } = useSelector((state) => state.stampReducer);
  const dispatch = useDispatch();

  const [formValues, setFormValues] = useState<IFormValues>({
    stampFile: null,
    categories: [],
    price: 10,
    releaseDate: null,
    stampName: '',
    stampType: stampTypes[0],
  });

  const [uploadProgress, setUploadProgress] = useState<number>(0);
  const [datePickerShown, setDatePickerShown] = useState(false);

  const [stampEditMessage, setStampEditMessage] = useState<{ message: string; error?: boolean }>({ message: '' });
  const [showToast, setShowToast] = useState(false);

  function preventDefault(event: React.DragEvent<HTMLDivElement> | React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    event.preventDefault();
  }

  useEffect(() => {
    if (!stamp) return;
    api.stampApi
      .getCategoriesByIds(stamp.categories)
      .then((res) => {
        setFormValues({
          stampFile: null,
          categories: stamp.categories
            .map((cat) => res.find((result) => result.id === cat)?.name)
            .filter((name) => Boolean(name)) as string[],
          price: stamp.price,
          releaseDate: new Date(stamp.releaseDate),
          stampName: stamp.name,
          stampType: stamp.stampType,
        });
      })
      .catch();
  }, [stamp]);

  useEffect(() => {
    dispatch(setCurrentlyViewedStamp(null));
    if (!stamps || stamps.length < 1) {
      dispatch(getStamps());
    }

    dispatch(refreshCategories());
  }, []);

  useEffect(() => {
    if (!stampEditMessage.message) setShowToast(false);
    else setShowToast(true);
  }, [stampEditMessage]);

  function uploadStamp(args: Omit<IUploadFileArgs, 'progressEventCallback'>) {
    api.stampApi
      .uploadStampFile({
        ...args,
        progressEventCallback: (evt) => {
          const { loaded, total } = evt;
          const percentage = Math.round((loaded * 100) / total);
          setUploadProgress(percentage);
        },
      })
      .then(() => {
        setFormValues({
          stampFile: null,
          categories: [],
          price: stamp?.price || 10,
          releaseDate: stamp?.releaseDate ? new Date(stamp.releaseDate) : null,
          stampName: stamp?.name || '',
          stampType: stamp?.stampType || stampTypes[0],
        });
        setUploadProgress(0);
      })
      .catch((err) => console.error(err));
  }

  function editStamp() {
    if (!formValues.categories || !formValues.stampName || !formValues.stampType || !stamp || !stamp.id) {
      return;
    }
    api.stampApi
      .editStamp(stamp.id, {
        categories: formValues.categories,
        name: formValues.stampName,
        stampType: formValues.stampType,
      })
      .then(() => {
        setStampEditMessage({ message: 'Edit sucessful!' });
      })
      .catch((err) => {
        if (err instanceof ApiError) {
          setStampEditMessage({ message: err.message, error: true });
        } else {
          setStampEditMessage({
            message: 'There was an error processing your request. Double check the values and try again.',
            error: true,
          });
        }
      });
  }

  function launchStampOnDate() {
    if (!stamp || !formValues.releaseDate) return;

    api.stampApi.launchStampOnDate(stamp.id, formValues.releaseDate).catch((err) => console.error(err));
  }

  return (
    <StampDetailContainer
      uploadProgress={uploadProgress}
      className={stampEditMessage.error ? 'error' : ''}
      onClick={() => setShowToast(false)}
    >
      <div className='title'>stamp settings</div>
      <form>
        <div
          className='dropfield'
          onDragOver={preventDefault}
          onDragEnter={preventDefault}
          onDrop={(e) => {
            e.preventDefault();
            setFormValues({ ...formValues, stampFile: e.dataTransfer.files[0] });
          }}
        >
          <div className='dropfieldcenter'>{formValues.stampFile?.name || 'Upload new file...'}</div>
        </div>
        <div className='horizontal'>
          <label className='formlabel' htmlFor='stamp-name'>
            <p>Stamp Name</p>
            <Input
              id='stamp-name'
              type={inputTypes.text}
              value={formValues.stampName}
              onChange={(e) => {
                const { value } = e.target;
                setFormValues({ ...formValues, stampName: value });
              }}
            />
          </label>
          <label className='formlabel' htmlFor='stamp-type'>
            <p>Stamp Type</p>
            <Select
              id='stamp-type'
              options={stampTypes.map((type, index) => {
                return {
                  key: `stamp-type-${type}-${index}`,
                  value: type,
                };
              })}
              value={formValues.stampType}
              onChange={(e) => {
                const { value } = e.target;
                setFormValues({ ...formValues, stampType: value });
              }}
            ></Select>
          </label>
        </div>
        <label className='formlabel' htmlFor='stamp-price'>
          <p>Price</p>
          <Select
            id='stamp-price'
            options={supportedPrices.map((price, index) => {
              return {
                key: `stamp-price-${price}-${index}`,
                value: `$${price.toFixed(2)}`,
              };
            })}
            value={`$${formValues.price.toFixed(2)}`}
            onChange={(e) => {
              const { value } = e.target;
              const price = Number(value.replace('$', ''));
              setFormValues({ ...formValues, price });
            }}
          ></Select>
        </label>
        <label className='formlabel'>
          <p>Categories</p>
          <InputWithPills
            pills={formValues.categories}
            onChange={(values) => {
              setFormValues({ ...formValues, categories: values });
            }}
            maxPills={5}
          />
        </label>
        <div className='horizontal'>
          <Button
            className='colored thick'
            displayText='Save'
            onClick={(evt) => {
              preventDefault(evt);
              // This is if we are uploading a new stamp
              if (!stamp) {
                if (!formValues.stampName || !formValues.stampType || !formValues.price || !formValues.stampFile)
                  return;
                uploadStamp({
                  price: formValues.price,
                  stamp: formValues.stampFile,
                  stampName: formValues.stampName,
                  stampType: formValues.stampType,
                  categories: formValues.categories,
                });
              }

              // This is if we are editing an existing stamp
              editStamp();
            }}
          />
          <div
            onMouseEnter={() => setDatePickerShown(true)}
            onMouseLeave={() => setDatePickerShown(false)}
            className='date-pick'
          >
            {datePickerShown && (
              <input
                type='date'
                onChange={(e) => {
                  const { valueAsDate } = e.target;
                  if (!valueAsDate) return;
                  const tzOffset = valueAsDate.getTimezoneOffset();

                  let newDate = new Date(valueAsDate.getTime());
                  newDate = new Date(newDate.setTime(newDate.getTime() + tzOffset * 60 * 1000));
                  console.log(newDate);

                  setFormValues({ ...formValues, releaseDate: newDate });
                }}
                value={
                  formValues.releaseDate
                    ? formValues.releaseDate.toISOString().split('T')[0]
                    : new Date().toISOString().split('T')[0]
                }
              />
            )}
            <Button
              className='important thick'
              displayText={`Launch on ${formValues.releaseDate ? formValues.releaseDate.toLocaleDateString() : '...'}`}
              onClick={(e) => {
                preventDefault(e);
                launchStampOnDate();
              }}
            />
          </div>
        </div>
      </form>
      {/* {stampEditMessage.message && <p className='stamp-edit-message'>{stampEditMessage.message}</p>} */}

      <Snackbar
        open={showToast}
        message={stampEditMessage.message}
        autoHideDuration={6000}
        onClose={() => setStampEditMessage({ message: '' })}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      />
    </StampDetailContainer>
  );
}
