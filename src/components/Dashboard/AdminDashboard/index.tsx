import React from 'react';
import { Route } from 'react-router';
import EditPackages from './EditPackages';
import MyStamps from './MyStamps';
import StampDetailView from './MyStamps/StampDetailView';

export default function AdminDashboard() {
  return (
    <>
      <Route path='/profile/upload' exact>
        <StampDetailView stamp={null} />
      </Route>

      <Route path='/profile/packages' exact>
        <EditPackages />
      </Route>

      <Route path='/profile/mystamps/:stampId?'>
        <MyStamps />
      </Route>
    </>
  );
}
