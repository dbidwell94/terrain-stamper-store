import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import { isAdmin } from '@src/utils';
import styled from 'styled-components';
import AdminDashboard from './AdminDashboard';
import UserDashboard from './UserDashboard';

const DashboardContainer = styled.div`
  grid-column: 2 / 6;
  grid-row: 2 / -1;
`;

export default function Dashboard() {
  const { currentUser } = useSelector((state) => state.authReducer);

  if (!currentUser) {
    return <Redirect to={'/'} />;
  }

  return <DashboardContainer>{isAdmin(currentUser) ? <AdminDashboard /> : <UserDashboard />}</DashboardContainer>;
}
