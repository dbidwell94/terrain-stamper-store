import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IStampFile, IStampView } from '@api/stampApi';
import ModalContainer from '@sc/ModalContainer';
import { setCurrentlyViewedStamp } from '@state/StampReducer/StampActions';
import styled from 'styled-components';
import Button from '@sc/Button';
import DropdownOptionsPopup from '@sc/DropdownOptionsPopup';
import { toggleAuthModal } from '@state/AuthReducer/AuthActions';
import { isAdmin } from '@src/utils';
import LoadingSvg from '@sc/Loading';
import { Link } from 'react-router-dom';
import { PaymentStatus } from '@api/userApi';
import api, { serverUrl } from '@api/index';
import CloseIcon from '@mui/icons-material/Close';
import EditIcon from '@mui/icons-material/EditOutlined';

const StampModalContainer = styled.div`
  z-index: 10;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  width: 960px;
  max-width: calc(100vw - 50px);
  height: 630px;
  max-height: calc(100vh - 50px);
  border-radius: 0.5rem;
  background: #2e2e2e;
  display: flex;
  flex-direction: column;
  box-shadow: 5px 5px 30px black;

  .content {
    height: 80%;
    background: #161616;
    position: relative;
    overflow: hidden;
    display: flex;
    img {
      transition: 1s ease-in-out all;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      object-fit: cover;
      height: auto;
      width: 100%;
      z-index: 1;
      filter: opacity(0);
      &.active {
        filter: opacity(100);
      }
      &.hidden {
        filter: opacity(0);
      }
    }
  }
`;

const StampBar = styled.div`
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  grid-template-rows: repeat(3, 1fr);
  padding: 2rem;
  height: 20%;
  * {
    display: flex;
  }
`;

const StampTitleDisplay = styled.section`
  flex-direction: column;
  grid-column: 1 / 4;
  grid-row: 1 / 3;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: flex-start;
  h3 {
    font-size: 2.8rem !important;
    text-transform: uppercase;
    font-weight: bold;
    color: white;
  }
  p {
    font-size: 1.2rem !important;
    text-transform: uppercase;
    font-weight: bold;
    color: rgb(122, 122, 122);
  }
`;

const StampButtonContainer = styled.div`
  grid-column: 5 / 7;
  grid-row: 1 / 3;
  justify-content: flex-end;
  align-items: center;
  .button-container {
    width: 80%;
    height: 80%;
  }
`;

const StampLinkContainer = styled.div`
  grid-column: 6 / 7;
  grid-row: 3 / 4;
  align-items: center;
  justify-content: flex-end;
  a {
    width: 2.5rem;
    height: 2.5rem;
    background: rgb(0, 135, 255);
    margin: 0;
    padding: 0;
    transition: 0.0625s ease-in-out all;
    &:hover {
      background: rgb(59, 59, 59);
    }
    img {
      width: 2.5rem;
      height: 2.5rem;
    }
  }
`;

const DownloadSelection = styled(Button)<{ arrowSize: number }>`
  && {
    position: relative;
    width: auto;
    background: grey;
    border-radius: ${({ theme }) => `${theme.global.padding / 1.5}rem`};
    border-top-left-radius: 0rem;
    border-bottom-left-radius: 0rem;
    border-left-color: black;
    border-left-style: solid;
    border-left-width: 0.125rem;
    &::after {
      content: '';
      position: absolute;
      width: ${({ arrowSize }) => arrowSize}rem;
      height: ${({ arrowSize }) => arrowSize}rem;
      top: 50%;
      left: 50%;
      border: 0.25rem solid white;
      border-right-width: 0rem;
      border-bottom-width: 0rem;
      transform: translate(-50%, calc(50% - ${({ arrowSize }) => arrowSize * 1.5}rem)) rotate(225deg);
    }
  }
`;

const DownloadButton = styled(Button)`
  && {
    border-top-right-radius: 0rem;
    border-bottom-right-radius: 0rem;
  }
`;

const DownloadOptionsContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  position: relative;
`;

const CloseButton = styled.button`
  position: absolute;
  z-index: 100;
  top: 1rem;
  left: calc(100% - 1rem);
  transform: translate(-100%, 0%);
  z-index: 1;
  height: 3rem;
  width: 3rem;
  background: ${({ theme }) => theme.global.colors.inputActive};
  border: none;
  outline: none;
  &:hover {
    background: ${({ theme }) => theme.global.colors.stampBackground};
  }
`;

interface IStampModalProps {
  stamp: IStampView;
}

const pictureChangeIntervalMs = 5000;

export default function StampModal(props: IStampModalProps) {
  const dispatch = useDispatch();

  const [purchaseAllowed, setPurchaseAllowed] = useState(true);

  const { stamp } = props;
  const { authModalOpen, token, currentUser } = useSelector((state) => state.authReducer);

  async function handlePurchaseRequest(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    event.preventDefault();
    setPurchaseAllowed(false);
    try {
      const link = await api.purchaseApi.getLinkToPurchaseStamp(stamp.id);
      window.location.href = link;
      setPurchaseAllowed(true);
    } catch (err) {
      setPurchaseAllowed(true);
    }
  }

  function downloadFile(fileLocation: string, fileName: string) {
    const linkElement = document.createElement('a');
    linkElement.style.display = 'none';
    linkElement.target = '_blank';
    linkElement.download = fileName;
    linkElement.href = fileLocation;
    document.body.appendChild(linkElement);
    linkElement.click();
    linkElement.remove();
  }

  const downloadOptions = useMemo(() => {
    return stamp.files.reduce<string[]>((options, file) => {
      const fileResolution = `${file.isUnityPackage ? 'Unity' : 'Raw'} ${file.resolution}`;
      return [...options, fileResolution];
    }, []);
  }, [stamp]);

  const userHasPurchasedFile = useMemo<boolean>(() => {
    if (!currentUser) return false;

    return currentUser.purchases.reduce<boolean>((hasPurchased, purchase) => {
      return hasPurchased || (purchase.paymentStatus === PaymentStatus.Paid && purchase.stampId == stamp.id);
    }, false);
  }, [stamp]);

  const [selectedDownloadOption, setSelectedDownloadOption] = useState<IStampFile | null>(null);
  const [downloadOptionsShown, setDownloadOptionsShown] = useState(false);

  const [currentPictureIndex, setCurrentPictureIndex] = useState(0);
  const [picturesLoaded, setPicturesLoaded] = useState(stamp.pictures.map(() => false));

  const allPicturesLoaded = useMemo<boolean>(() => {
    return picturesLoaded.every((picLoaded) => picLoaded === true);
  }, [picturesLoaded]);

  useEffect(() => {
    if (allPicturesLoaded) {
      const timeout = setTimeout(() => {
        const newIndex = currentPictureIndex === stamp.pictures.length - 1 ? 0 : currentPictureIndex + 1;
        setCurrentPictureIndex(newIndex);
      }, pictureChangeIntervalMs);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [currentPictureIndex, picturesLoaded]);

  return (
    <ModalContainer onClick={() => dispatch(setCurrentlyViewedStamp(null))}>
      <StampModalContainer>
        <div className='content'>
          {stamp.pictures.length > 0 &&
            stamp.pictures.map((picture, index) => {
              return (
                <img
                  src={`${serverUrl}/api/pictures/picture/${picture.id}`}
                  className={`${index === currentPictureIndex ? 'active' : 'hidden'} ${
                    allPicturesLoaded ? '' : 'hidden'
                  }`}
                  key={`stamp-picture-${picture.id}-${index}`}
                  onLoad={() => {
                    setPicturesLoaded(
                      picturesLoaded.map((value, pictureIndex) => {
                        if (pictureIndex === index) {
                          return true;
                        }
                        return value;
                      })
                    );
                  }}
                  alt={`Detailed picture of stamp ${stamp.name}`}
                />
              );
            })}
          {!allPicturesLoaded && <LoadingSvg className='active' />}
        </div>

        <CloseButton onClick={() => dispatch(setCurrentlyViewedStamp(null))}>
          <p style={{ display: 'none' }}>Close Modal</p>
          <CloseIcon style={{ height: '100%', width: '100%' }} />
        </CloseButton>

        <StampBar>
          <StampTitleDisplay>
            <h3>{stamp.name}</h3>
            <p>{stamp.stampType}</p>
          </StampTitleDisplay>

          <StampLinkContainer>
            {isAdmin(currentUser) && (
              <Link to={'/profile/mystamps/' + stamp.id} className='editbutton'>
                <EditIcon style={{ width: '100%', height: '100%' }} />
                {/* <img src={edit}></img> */}
              </Link>
            )}
          </StampLinkContainer>

          <StampButtonContainer>
            <div className='button-container'>
              {token ? (
                <DownloadOptionsContainer>
                  {userHasPurchasedFile ? (
                    <>
                      <DownloadButton
                        displayText={
                          selectedDownloadOption
                            ? `${selectedDownloadOption.isUnityPackage ? 'Unity' : 'Raw'} - ${
                                selectedDownloadOption.resolution
                              }`
                            : 'Select a resolution...'
                        }
                        onClick={() => {
                          if (!selectedDownloadOption) return;
                          api.purchaseApi
                            .downloadStampFile(stamp, selectedDownloadOption)
                            .then(([link, name]) => downloadFile(link, name));
                        }}
                        fullWidth
                        fullHeight
                      />
                      <DownloadSelection arrowSize={1} onClick={() => setDownloadOptionsShown(!downloadOptionsShown)} />
                      <DropdownOptionsPopup
                        options={downloadOptions}
                        enabled={downloadOptionsShown}
                        onChange={(resolution) => {
                          const sf = stamp.files.find((stampFile) => {
                            const isUnityPackage = resolution.split(' ')[0].toLocaleLowerCase().includes('unity');
                            const fileResolution = resolution.split(' ')[1];

                            return (
                              stampFile.resolution === fileResolution && stampFile.isUnityPackage === isUnityPackage
                            );
                          });

                          if (!sf) return;

                          setSelectedDownloadOption(sf);
                          setDownloadOptionsShown(false);
                        }}
                      />
                    </>
                  ) : (
                    <Button
                      fullWidth
                      fullHeight
                      displayText='Purchase'
                      onClick={handlePurchaseRequest}
                      disabled={!purchaseAllowed}
                    />
                  )}
                </DownloadOptionsContainer>
              ) : (
                <Button
                  className='sidebutton'
                  displayText='sign in to download'
                  onClick={() => dispatch(toggleAuthModal(!authModalOpen))}
                />
              )}
            </div>
          </StampButtonContainer>
        </StampBar>
      </StampModalContainer>
    </ModalContainer>
  );
}
