import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Sidebar from '@components/Sidebar';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import FilterBar from '@components/FilterBar';
import ContentFeed from '@components/ContentFeed';
import { useSelector } from 'react-redux';
import AuthModal from '@components/AuthModal';
import Dashboard from '@components/Dashboard';
import Loading from '@sc/Loading';
import StampModal from '@components/StampModal';
import api from '@api/index';
import ChangePassword from '@components/ChangePassword';

const HomePage = styled.div`
  display: grid;
  height: 100vh;
  width: 100%;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(9, 1fr);
  *::-webkit-scrollbar {
    width: 10px;
    border-radius: 5px;
    background: transparent;
    transition: 0.25s ease-in-out all;
  }
  *::-webkit-scrollbar-thumb {
    background: rgb(200, 200, 200);
    border-radius: 5px;
    transition: 0.25s ease-in-out all;
  }
  *::-webkit-scrollbar-thumb:hover {
    background: grey;
    transition: 0.25s ease-in-out all;
  }
  *::-webkit-scrollbar-thumb:active {
    background: rgb(100, 100, 100);
  }
`;

const FullPageLoading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
`;

export interface IContentFilter {
  type: 'road' | 'terrain' | 'all types';
  resolutions: 'all resolutions' | string;
  status: 'all stamps' | 'new' | 'purchased';
}

export default function App() {
  const { authModalOpen, currentUser, token } = useSelector((state) => state.authReducer);
  const { currentlyOpenedStamp } = useSelector((state) => state.stampReducer);

  const [searchPills, setSearchPills] = useState<string[]>([]);
  const [filters, setFilters] = useState<IContentFilter>({
    resolutions: 'all resolutions',
    status: 'all stamps',
    type: 'all types',
  });

  const queryParams = (() => {
    const paramsToReturn: Record<string, string> = {};
    const params = new URLSearchParams(location.search);
    for (const [key, value] of params.entries()) {
      paramsToReturn[key] = value;
    }
    return paramsToReturn;
  })();

  useEffect(() => {
    api.userApi.updateCurrentUser();
  }, []);

  if (token && !currentUser) {
    return (
      <FullPageLoading>
        <Loading />
      </FullPageLoading>
    );
  }

  return (
    <Router>
      {authModalOpen && <AuthModal />}
      {queryParams.forgotpassword && queryParams.token && <ChangePassword token={queryParams.token} />}
      {currentlyOpenedStamp && <StampModal stamp={currentlyOpenedStamp} />}
      <HomePage>
        <FilterBar setSearchbarText={setSearchPills} filters={filters} setFilters={setFilters} />
        <Switch>
          <Route path='/profile/(upload|mystamps|purchases)?'>
            <Dashboard />
          </Route>
          <Route path='/forgotpassword'>
            {queryParams.token ? (
              <Redirect to={`?forgotpassword=true&token=${queryParams.token}`} />
            ) : (
              <Redirect to='/' />
            )}
          </Route>
          <Route path='/' exact>
            <ContentFeed searchbarPills={searchPills} filter={filters} />
          </Route>
        </Switch>
        <Sidebar />
      </HomePage>
    </Router>
  );
}
