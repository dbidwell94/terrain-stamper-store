import React, { useEffect, useState } from 'react';
import { IServerCategory } from 'src/api/stampApi';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';

const ItemContainer = styled.div`
  height: min-content;
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: ${({ theme }) => theme.global.padding / 4}rem 0rem;
  padding-left: ${({ theme }) => theme.global.padding}rem;
  padding-bottom: 0rem;
  border-left: 2px solid #ffffff;
  user-select: none;
  overflow: show;
  align-items: flex-start;
  transition: 0.25s ease-in-out all;

  p {
    position: relative;
    color: grey;
    text-transform: capitalize;
    &:hover {
      color: white;
    }
    &::after {
      display: none;
      position: absolute;
      left: calc(0% - 2.2rem);
      top: calc(0% - 0.5rem);
      content: ' ';
      height: calc(100% + 0.5rem);
      border-left: 0.2rem solid ${({ theme }) => theme.global.colors.inputActive};
    }
    &.selected {
      &::after {
        display: block;
      }
      color: white;
    }
  }

  div.children {
    transition: 0.25s ease-in-out all;
    height: 0%;
    display: flex;
    overflow: hidden;
    flex-direction: column;
    max-height: 0rem;
    &.opened {
      height: 100%;
      display: flex;
      max-height: 100%;
    }
  }
`;

interface ICategoryItemProps {
  name: string;
  parentOpened: boolean;
  content: IServerCategory;
  setSelectedCategoryKey: React.Dispatch<React.SetStateAction<string | null>>;
  selectedCategoryKey: string | null;
  graphList: string[];
  setGraphList: React.Dispatch<React.SetStateAction<string[]>>;
  nestedIndex: number;
}

export default function CategoryItem(props: ICategoryItemProps) {
  const { parentOpened, content, setSelectedCategoryKey, selectedCategoryKey, graphList, setGraphList, nestedIndex } =
    props;
  const [myKey] = useState(JSON.stringify({ key: uuid(), name: content.name, id: content.id }));
  const [opened, setOpened] = useState<boolean>(Boolean(myKey === selectedCategoryKey));

  useEffect(() => {
    if (!graphList.includes(myKey)) {
      setOpened(false);
    }
  }, [graphList]);

  useEffect(() => {
    if (opened) {
      let newGraphList: string[];
      if (graphList.length > nestedIndex) {
        newGraphList = [...graphList.slice(0, nestedIndex), myKey];
      } else {
        newGraphList = [...graphList, myKey];
      }
      setGraphList(newGraphList);
    } else {
      if (nestedIndex < 1) {
        setGraphList([]);
      }
    }
  }, [opened]);

  useEffect(() => {
    if (!parentOpened) {
      setOpened(false);
      if (myKey === selectedCategoryKey) {
        setSelectedCategoryKey(null);
      }
    }
  }, [parentOpened]);

  function onClick(evt: React.MouseEvent<HTMLParagraphElement, MouseEvent>) {
    evt.preventDefault();
    evt.stopPropagation();
    setOpened(!opened);
    setSelectedCategoryKey(myKey);
  }

  return (
    <ItemContainer>
      <p onClick={onClick} className={selectedCategoryKey === myKey ? 'selected' : ''}>
        {props.name}
      </p>
      <div className={`children${opened ? ' opened' : ''}`}>
        {Object.values(content.children).map((child, index) => {
          return (
            <CategoryItem
              content={child}
              name={child.name}
              parentOpened={Boolean(opened)}
              key={`sidebar${content.name}${child.name}${index}`}
              selectedCategoryKey={selectedCategoryKey}
              setSelectedCategoryKey={setSelectedCategoryKey}
              graphList={graphList}
              setGraphList={setGraphList}
              nestedIndex={nestedIndex + 1}
            />
          );
        })}
      </div>
    </ItemContainer>
  );
}
