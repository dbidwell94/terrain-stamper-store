import React, { useEffect, useState } from 'react';
import { IServerCategory } from '@api/stampApi';
import styled from 'styled-components';
import { categoryNames } from '../index';
import CategoryItem from './CategoryItem';

const CategoryHeaderContainer = styled.div`
  padding: ${({ theme }) => `${theme.global.padding}rem`};
  padding-bottom: 0rem;
  color: #b6b6b6;
  width: 100%;
  height: 100%;

  &:hover {
    color: white;
  }

  &.active {
    background: linear-gradient(to right, #0087ff 5px, transparent 5px);
  }

  .imgcontainer {
    display: block;
    width: 5rem;
    height: 5rem;
    position: relative;

    img {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translateX(-50%) translateY(-50%);
      width: 70%;
    }
  }

  label {
    font-size: 12pt;
    font-weight: bold;
    text-transform: capitalize;
    width: 100%;
  }

  .assetcount {
    color: gray;
    float: right;
    font-weight: normal;
    text-align: right;
  }

  div.main-container {
    width: 100%;
    display: grid;
    grid-template-columns: calc(5rem + ${({ theme }) => `${theme.global.padding}rem`}) auto auto;
    align-items: center;
  }

  div.assets {
    grid-column: 1/3;
    display: flex;
    flex-direction: column;
    padding-left: 2.5rem;
    transition: 0.5s ease-in-out all;
    height: 0%;
    max-height: 0rem;
    width: 100%;
    overflow: hidden;
    &.opened {
      display: flex;
      height: 100%;
      max-height: 100%;
    }
  }
`;

interface ICategotyProps {
  categoryName: categoryNames;
  categoryIcon: string;
  opened?: boolean;
  onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  onGraphUpdate?: (arg: number[]) => void;
  content: IServerCategory[];
  assetCount: number;
}

export default function CategoryHeader(props: ICategotyProps) {
  const { content, opened, onGraphUpdate, assetCount } = props;
  const [selectedCategoryKey, setSelectedCategoryKey] = useState<string | null>(null);
  const [selectedGraphList, setSelectedGraphList] = useState<string[]>([]);

  useEffect(() => {
    if (onGraphUpdate) {
      onGraphUpdate(selectedGraphList.map((list) => JSON.parse(list).id).filter((id) => Boolean(id)));
    }
  }, [selectedGraphList]);

  return (
    <CategoryHeaderContainer className={props.opened ? 'active' : ''}>
      <div className='main-container' onClick={props.onClick}>
        <div className='imgcontainer'>
          <img src={props.categoryIcon} />
        </div>
        <label>{props.categoryName}</label>
        <label className='assetcount'>{assetCount}</label>
      </div>

      <div className={`assets ${props.opened ? 'opened' : ''}`}>
        {content.map((item, index) => {
          return (
            <CategoryItem
              parentOpened={Boolean(opened)}
              key={`cat${item}${index}`}
              content={item}
              name={item.name}
              setSelectedCategoryKey={setSelectedCategoryKey}
              selectedCategoryKey={selectedCategoryKey}
              graphList={selectedGraphList}
              setGraphList={setSelectedGraphList}
              nestedIndex={0}
            />
          );
        })}
      </div>
    </CategoryHeaderContainer>
  );
}
