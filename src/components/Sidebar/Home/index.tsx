import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import home from '@src/assets/home.png';
import gift from '@src/assets/gift.png';
import bag from '@src/assets/bag.png';

import CategoryHeaderContainer from './CategoryHeader';
import { useDispatch, useSelector } from 'react-redux';
import { getUserPurchasedStamps, refreshCategories, updateCategoryFilter } from '@src/state/StampReducer/StampActions';
import api from '@api/index';

const SidebarContainer = styled.div`
  grid-column: 1 / 1;
  grid-row: 1 / 7;
  background-color: #151515;
  display: flex;
  flex-direction: column;
`;

export enum categoryNames {
  home = 'home',
  collections = 'collections',
  myAssets = 'my assets',
}

export default function Sidebar({ className }: { className?: string }) {
  const [selectedItem, setSelectedItem] = useState<null | categoryNames>(null);

  const { categories } = useSelector((state) => state.stampReducer);

  const [categoryGraph, setCategoryGraph] = useState<number[]>([]);
  const [_packageGraph, setPackageGraph] = useState<number[]>([]);
  const [_purchaseGraph, setPurchaseGraph] = useState<number[]>([]);

  const [categoryAssetCount, setCategoryAssetCount] = useState<number>(0);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateCategoryFilter(categoryGraph));
    api.stampApi
      .getStampAmountByCategoryList(categoryGraph)
      .then((res) => setCategoryAssetCount(res))
      .catch(console.error);
  }, [categoryGraph]);

  useEffect(() => {
    dispatch(getUserPurchasedStamps());
    dispatch(refreshCategories());
  }, []);

  function toggleCategoryHandler(name: categoryNames) {
    if (name === selectedItem) {
      setSelectedItem(null);
    } else {
      setSelectedItem(name);
    }
  }

  return (
    <SidebarContainer className={className}>
      {/* Categories */}
      <CategoryHeaderContainer
        categoryName={categoryNames.home}
        categoryIcon={home}
        opened={selectedItem === categoryNames.home}
        onClick={() => toggleCategoryHandler(categoryNames.home)}
        onGraphUpdate={(nums) => setCategoryGraph(nums)}
        content={categories}
        assetCount={categoryAssetCount}
      />

      {/* Packages */}
      <CategoryHeaderContainer
        categoryName={categoryNames.collections}
        categoryIcon={gift}
        opened={selectedItem === categoryNames.collections}
        onClick={() => toggleCategoryHandler(categoryNames.collections)}
        onGraphUpdate={(nums) => setPackageGraph(nums)}
        content={[]}
        assetCount={0}
      />

      {/* Purchases */}
      <CategoryHeaderContainer
        categoryName={categoryNames.myAssets}
        categoryIcon={bag}
        opened={selectedItem === categoryNames.myAssets}
        onClick={() => toggleCategoryHandler(categoryNames.myAssets)}
        onGraphUpdate={(nums) => setPurchaseGraph(nums)}
        content={[]}
        assetCount={0}
      />
    </SidebarContainer>
  );
}
