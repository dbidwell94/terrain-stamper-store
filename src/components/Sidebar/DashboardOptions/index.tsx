import React from 'react';
import { useSelector } from 'react-redux';
import { isAdmin } from 'src/utils';
import styled from 'styled-components';
import AdminOptions from './AdminOptions';
import UserOptions from './UserOptions';

const OptionsContainer = styled.div`
  .option {
    margin-bottom: 1rem;
  }
`;

export default function DashboardOptions() {
  const { currentUser } = useSelector((state) => state.authReducer);

  return <OptionsContainer>{isAdmin(currentUser) ? <AdminOptions /> : <UserOptions />}</OptionsContainer>;
}
