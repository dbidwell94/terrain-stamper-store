import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const DivContainer = styled.div`
  width: 100%;
  color: white;
  cursor: pointer;
  background: grey;
  text-align: center;
`;

const LinkContainer = styled(NavLink)`
  width: 100%;
  color: white;
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  background: grey;
  transition: 0.125s ease-in-out all;
  &:hover {
    background: white;
    color: black;
  }
`;

interface IDashboardDivOptionProps {
  optionName: string;
  onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  isLink: false;
}

interface IDashboardNavLinkOptionProps {
  optionName: string;
  to: string;
  isLink: true;
}

type IDashboardOptionProps = IDashboardDivOptionProps | IDashboardNavLinkOptionProps;

export default function DashboardOption(props: IDashboardOptionProps) {
  return (
    <>
      {props.isLink ? (
        <LinkContainer to={props.to} className='option'>
          {props.optionName}
        </LinkContainer>
      ) : (
        <DivContainer onClick={props.onClick} className='option'>
          <p>{props.optionName}</p>
        </DivContainer>
      )}
    </>
  );
}
