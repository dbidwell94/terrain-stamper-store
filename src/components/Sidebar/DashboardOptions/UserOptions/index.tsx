import React from 'react';
import DashboardOption from '../DashboardOption';

export default function UserOptions() {
  return (
    <React.Fragment>
      <DashboardOption isLink to='/profile/purchases' optionName='Purchases' />
    </React.Fragment>
  );
}
