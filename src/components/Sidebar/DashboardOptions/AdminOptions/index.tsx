import React from 'react';
import DashboardOption from '../DashboardOption';

export default function AdminOptions() {
  return (
    <React.Fragment>
      <DashboardOption optionName='Upload' isLink={true} to='/profile/upload' />
      <DashboardOption optionName='Edit Packages' isLink={true} to='/profile/packages' />
    </React.Fragment>
  );
}
