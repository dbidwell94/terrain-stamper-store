import React from 'react';
import styled from 'styled-components';
import logo from 'src/assets/atlas.svg';
import { useHistory } from 'react-router';

const LogoContainer = styled.div`
  padding: ${({ theme }) => `${theme.global.padding}rem`};
  cursor: pointer;

  color: #b6b6b6;

  grid-row: 1 / 2;
  grid-column: 1 / 1;
  align-items: center;

  display: grid;
  grid-template-columns: calc(5rem + ${({ theme }) => `${theme.global.padding}rem`}) auto auto;
  transition: 0.125s ease-in-out all;

  img {
    height: 5rem;
    width: 5rem;
  }

  label {
    font-size: 20pt;
    font-weight: bold;
    text-transform: uppercase;
    width: 100%;
    cursor: pointer;
  }
`;

interface ILogoProps {
  className?: string;
}

export default function Logo({ className }: ILogoProps) {
  const { push } = useHistory();

  return (
    <LogoContainer className={className} onClick={() => push('/')}>
      <img src={logo}></img>
      <label>atlas</label>
    </LogoContainer>
  );
}
