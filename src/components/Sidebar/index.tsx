import React from 'react';
import styled from 'styled-components';
import { Route } from 'react-router-dom';
import HomeSidebar from './Home';
import DashboardSidebar from './DashboardOptions';
import Logo from './Logo';

const SidebarContainer = styled.div`
  grid-column: 1 / 1;
  grid-row: 1 / -1;
  background-color: #151515;
  display: flex;
  flex-direction: column;
`;

export default function Sidebar() {
  return (
    <SidebarContainer>
      <Logo />
      <Route path={'/profile/:view(upload|edit)?/:stampId?'}>
        <DashboardSidebar />
      </Route>
      <Route path='/' exact>
        <HomeSidebar />
      </Route>
    </SidebarContainer>
  );
}
