import { Action } from 'redux';
import { IPackage } from 'src/api/stampApi';

export enum packageActionTypes {
  SET_PACKAGES = 'SET_PACKAGES',
  SET_USER_PURCHASED_PACKAGES = 'SET_USER_PURCHASED_PACKAGES',
  SET_PACKAGES_LOADING = 'SET_PACKAGES_LOADING',
  SET_PACKAGE_LOADING_ERROR = 'SET_PACKAGE_LOADING_ERROR',
  SET_CURRENTLY_LOADED_PACKAGE = 'SET_CURRENTLY_LOADED_PACKAGE',
}

export interface IPackageState {
  packages: IPackage[];
  purchasedPackages: IPackage[];
  packagesLoadingCount: number;
  packageLoadingErrors: string[];
  currentlyViewingPackage: IPackage | null;
}

interface SetPackageAction extends Action {
  type: packageActionTypes.SET_PACKAGES;
  payload: IPackage[];
}

interface SetPurchasedPackageAction extends Action {
  type: packageActionTypes.SET_USER_PURCHASED_PACKAGES;
  payload: IPackage[];
}

interface SetPackagesLoadingAction extends Action {
  type: packageActionTypes.SET_PACKAGES_LOADING;
  payload: number;
}

interface SetPackageLoadingErrorAction extends Action {
  type: packageActionTypes.SET_PACKAGE_LOADING_ERROR;
  payload: string[];
}

interface SetCurrentlyViewedPackageAction extends Action {
  type: packageActionTypes.SET_CURRENTLY_LOADED_PACKAGE;
  payload: IPackage | null;
}

export type IPackageAction =
  | SetPackageAction
  | SetPurchasedPackageAction
  | SetPackagesLoadingAction
  | SetPackageLoadingErrorAction
  | SetCurrentlyViewedPackageAction;
