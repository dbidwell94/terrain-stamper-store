import api from 'src/api';
import { ApiError } from 'src/api/apiClient';
import { IThunkAction } from '..';
import { IPackageAction, packageActionTypes } from './types';

export function getPackages(): IThunkAction<IPackageAction> {
  return async function (dispatch, getState) {
    try {
      dispatch({
        type: packageActionTypes.SET_PACKAGES_LOADING,
        payload: getState().packageReducer.packagesLoadingCount + 1,
      });

      const packages = await api.packageApi.getAllPackages();

      dispatch({
        type: packageActionTypes.SET_PACKAGES,
        payload: packages,
      });
    } catch (err) {
      if (err instanceof ApiError) {
        const { packageLoadingErrors } = getState().packageReducer;

        dispatch({
          type: packageActionTypes.SET_PACKAGE_LOADING_ERROR,
          payload: [...packageLoadingErrors, err.message],
        });
      }
    } finally {
      dispatch({
        type: packageActionTypes.SET_PACKAGES_LOADING,
        payload: getState().packageReducer.packagesLoadingCount - 1,
      });
    }
  };
}

export function getUserPurchasedPackages(): IThunkAction<IPackageAction> {
  return async function (dispatch, getState) {
    try {
      dispatch({
        type: packageActionTypes.SET_PACKAGES_LOADING,
        payload: getState().stampReducer.stampsLoadingCalls + 1,
      });

      const packages = await api.packageApi.getUserPurchasedPackages();
      dispatch({
        type: packageActionTypes.SET_USER_PURCHASED_PACKAGES,
        payload: packages,
      });
    } catch (err) {
      if (err instanceof ApiError) {
        const { stampLoadingErrors } = getState().stampReducer;
        dispatch({
          type: packageActionTypes.SET_PACKAGE_LOADING_ERROR,
          payload: [...stampLoadingErrors, err.message],
        });
      }
    } finally {
      dispatch({
        type: packageActionTypes.SET_PACKAGES_LOADING,
        payload: getState().stampReducer.stampsLoadingCalls - 1,
      });
    }
  };
}

export function setCurrentlyViewedPackage(packageId: number | null): IThunkAction<IPackageAction> {
  return function (dispatch, getState) {
    const { packages } = getState().packageReducer;

    if (!packageId) {
      dispatch({
        type: packageActionTypes.SET_CURRENTLY_LOADED_PACKAGE,
        payload: null,
      });
      return;
    }

    const pkg = packages.find((p) => p.id === packageId);
    if (!pkg) {
      dispatch({
        type: packageActionTypes.SET_CURRENTLY_LOADED_PACKAGE,
        payload: null,
      });
      return;
    }
    dispatch({
      type: packageActionTypes.SET_CURRENTLY_LOADED_PACKAGE,
      payload: pkg,
    });
  };
}
