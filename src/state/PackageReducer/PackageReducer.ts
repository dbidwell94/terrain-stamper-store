import { IPackageAction, IPackageState, packageActionTypes } from './types';

const initialState: IPackageState = {
  packages: [],
  packagesLoadingCount: 0,
  purchasedPackages: [],
  packageLoadingErrors: [],
  currentlyViewingPackage: null,
};

export default function (state = initialState, action: IPackageAction): IPackageState {
  switch (action.type) {
    case packageActionTypes.SET_PACKAGES:
      return { ...state, packages: action.payload };

    case packageActionTypes.SET_PACKAGES_LOADING:
      return { ...state, packagesLoadingCount: action.payload };

    case packageActionTypes.SET_USER_PURCHASED_PACKAGES:
      return { ...state, purchasedPackages: action.payload };

    case packageActionTypes.SET_PACKAGE_LOADING_ERROR:
      return { ...state, packageLoadingErrors: action.payload };

    case packageActionTypes.SET_CURRENTLY_LOADED_PACKAGE:
      return { ...state, currentlyViewingPackage: action.payload };

    default:
      return state;
  }
}
