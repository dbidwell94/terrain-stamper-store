import { Action } from 'redux';
import { IUser } from '../../api/userApi';

export enum authTypes {
  SET_AUTH_MODAL_OPEN = 'SET_AUTH_MODAL_OPEN',
  SET_CURRENT_USER = 'SET_CURRENT_USER',
  SET_TOKEN = 'SET_TOKEN',
  SET_IS_AUTH_LOADING = 'SET_IS_AUTH_LOADING',
  ADD_AUTH_ERROR = 'ADD_AUTH_ERROR',
  REMOVE_AUTH_ERROR = 'REMOVE_AUTH_ERROR',
}

interface IAuthError {
  message: string;
  key: string;
}

export interface IAuthState {
  authModalOpen: boolean;
  currentUser: IUser | null;
  token: string | null;
  isLoading: boolean;
  errors: IAuthError[];
}

interface ErrorAction extends Action {
  type: authTypes.ADD_AUTH_ERROR | authTypes.REMOVE_AUTH_ERROR;
  payload: IAuthError;
}

interface ModalAction extends Action {
  type: authTypes.SET_AUTH_MODAL_OPEN | authTypes.SET_IS_AUTH_LOADING;
  payload: boolean;
}

interface UserAction extends Action {
  type: authTypes.SET_CURRENT_USER;
  payload: IUser | null;
}

interface TokenAction extends Action {
  type: authTypes.SET_TOKEN;
  payload: string | null;
}

export type IAuthAction = ModalAction | UserAction | TokenAction | ErrorAction;
