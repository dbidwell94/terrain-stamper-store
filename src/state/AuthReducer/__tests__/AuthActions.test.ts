import { logUserIn, logUserOut, registerUser, setCurrentUser, setToken } from '@state/AuthReducer/AuthActions';
import userApi, { IRegisterOptions, IUser } from '@api/userApi';
import { mocked } from 'jest-mock';
import createMockStore from 'redux-mock-store';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { IGlobalState } from '@src/state';
import { authTypes } from '../types';
import api from '@src/api';
import { ApiError } from '@src/api/apiClient';
import { AxiosError } from 'axios';
import { stampActionTypes } from '@src/state/StampReducer/types';

const middlewares = [thunk];
type DispatchExts = ThunkDispatch<IGlobalState, void, AnyAction>;
const mockStore = createMockStore<IGlobalState, DispatchExts>(middlewares);

const fakeLocalStorage: Storage = {
  ...window.localStorage,
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn(),
  removeItem: jest.fn(),
};

const vanillaLocalStorage = window.localStorage;

jest.mock('@api/userApi', () => {
  return jest.fn().mockImplementation(() => {
    return {
      login: jest.fn(async (username, ..._) => {
        const mockedUser: IUser = {
          createdAt: '',
          updatedAt: '',
          username,
          email: 'randomemail@gmail.com',
          id: 1,
          purchases: [],
          roles: [],
        };
        return mockedUser;
      }),
      register: jest.fn(),
    };
  });
});

beforeEach(() => {
  Object.defineProperty(window, 'localStorage', {
    value: fakeLocalStorage,
  });

  mocked(userApi).mockClear();
});

afterEach(() => {
  Object.defineProperty(window, 'localStorage', {
    value: vanillaLocalStorage,
  });
});

describe('src/state/AuthReducer/AuthActions.ts', () => {
  describe('logUserIn()', () => {
    it('logs the user in with valid login', async () => {
      const expectedActions = [
        authTypes.SET_IS_AUTH_LOADING,
        authTypes.SET_CURRENT_USER,
        authTypes.SET_AUTH_MODAL_OPEN,
        authTypes.SET_IS_AUTH_LOADING,
      ];
      const store = mockStore();
      await store.dispatch(logUserIn('testPerson', 'testPassword') as any as AnyAction);
      const actualActions = store.getActions().map((action) => action.type);

      expect(actualActions).toEqual(expectedActions);
    });

    it('fails to log user in with invalid login', async () => {
      mocked(api.userApi.login).mockImplementationOnce(async () => {
        throw new ApiError('test', {} as AxiosError);
      });

      const expectedActions = [authTypes.SET_IS_AUTH_LOADING, authTypes.ADD_AUTH_ERROR, authTypes.SET_IS_AUTH_LOADING];
      const store = mockStore();
      await store.dispatch(logUserIn('testPerson', 'testPassword') as any as AnyAction);
      const actualActions = store.getActions().map((action) => action.type);

      expect(actualActions).toEqual(expectedActions);
    });
  });

  describe('logUserOut()', () => {
    it('removes the JWT from localStorage', async () => {
      const store = mockStore();
      await store.dispatch(logUserOut() as unknown as AnyAction);
      expect(mocked(window.localStorage.removeItem)).toHaveBeenCalledTimes(1);
    });

    it('dispatches the correct actions', async () => {
      const expectedActions = [
        authTypes.SET_TOKEN,
        authTypes.SET_CURRENT_USER,
        stampActionTypes.SET_USER_PURCHASED_STAMPS,
      ];
      const store = mockStore();
      await store.dispatch(logUserOut() as unknown as AnyAction);
      const actions = store.getActions().map((action) => action.type);
      expect(actions).toEqual(expectedActions);
    });
  });

  describe('setToken()', () => {
    it('dispatches the correct action', async () => {
      const store = mockStore();

      const fakeToken = 'fake_token';

      store.dispatch(setToken(fakeToken));

      expect(store.getActions()).toEqual([{ type: authTypes.SET_TOKEN, payload: fakeToken }]);
    });
  });

  describe('setCurrentUser()', () => {
    it('dispatches the correct action', async () => {
      const fakeUser: IUser = {
        createdAt: '',
        updatedAt: '',
        email: 'fakeemail@gmail.com',
        id: 1,
        purchases: [],
        roles: [{ id: 1, roleName: 'admin' }],
        username: 'testUser',
      };
      const store = mockStore();
      store.dispatch(setCurrentUser(fakeUser));
      expect(store.getActions()).toEqual([{ type: authTypes.SET_CURRENT_USER, payload: fakeUser }]);
    });
  });

  describe('registerUser()', () => {
    it('calls the register endpoint with the correct data', async () => {
      const expectedActions = [
        authTypes.SET_IS_AUTH_LOADING,
        authTypes.SET_CURRENT_USER,
        authTypes.SET_AUTH_MODAL_OPEN,
        authTypes.SET_IS_AUTH_LOADING,
      ];

      const fakeUser: IUser = {
        createdAt: '',
        updatedAt: '',
        email: 'fakeemail@gmail.com',
        id: 1,
        purchases: [],
        roles: [{ id: 1, roleName: 'admin' }],
        username: 'testUser',
      };

      mocked(api.userApi.register)
        .mockImplementationOnce(async (opts: IRegisterOptions) => {
          return {
            ...fakeUser,
            username: opts.username,
          } as IUser;
        })
        .mockImplementationOnce(async () => {
          throw new ApiError('testing', {} as AxiosError);
        });
      const registerOptions: IRegisterOptions = {
        email: 'testuser@gmail.com',
        password: 'testing123',
        roles: [{ roleName: 'user' }],
        username: 'testUser',
      };
      const store = mockStore();
      await store.dispatch(
        registerUser(registerOptions.username, registerOptions.email, registerOptions.password) as unknown as AnyAction
      );

      const actionTypes = store.getActions().map((action) => action.type);
      expect(actionTypes).toEqual(expectedActions);
      expect(mocked(api.userApi.register)).toHaveBeenCalledTimes(1);
      expect(mocked(api.userApi.register)).toHaveBeenCalledWith(
        expect.objectContaining({
          username: expect.stringMatching(registerOptions.username),
          password: expect.stringMatching(registerOptions.password),
          roles: [],
          email: expect.stringMatching(registerOptions.email),
        })
      );
    });

    it('creates an error when the request fails', async () => {
      const expectedActions = [authTypes.SET_IS_AUTH_LOADING, authTypes.ADD_AUTH_ERROR, authTypes.SET_IS_AUTH_LOADING];

      mocked(api.userApi.register).mockImplementation(async () => {
        throw new ApiError('Error', {} as AxiosError);
      });

      const registerOpts: IRegisterOptions = {
        email: 'testing@gmail.com',
        password: 'testing123',
        roles: [],
        username: 'testUser',
      };

      const store = mockStore();
      await store.dispatch(
        registerUser(registerOpts.username, registerOpts.email, registerOpts.password) as unknown as AnyAction
      );

      const actionTypes = store.getActions().map((action) => action.type);
      expect(actionTypes).toEqual(expectedActions);
    });
  });
});
