import { IAuthState, IAuthAction, authTypes } from './types';

const initialState: IAuthState = {
  authModalOpen: false,
  currentUser: null,
  token: null,
  isLoading: false,
  errors: [],
};

export default function (state = initialState, action: IAuthAction): IAuthState {
  switch (action.type) {
    case authTypes.SET_AUTH_MODAL_OPEN:
      return { ...state, authModalOpen: action.payload };

    case authTypes.SET_CURRENT_USER:
      return { ...state, currentUser: action.payload };

    case authTypes.SET_TOKEN:
      return { ...state, token: action.payload };

    case authTypes.SET_IS_AUTH_LOADING:
      return { ...state, isLoading: action.payload };

    case authTypes.ADD_AUTH_ERROR:
      return { ...state, errors: [...state.errors, action.payload] };

    case authTypes.REMOVE_AUTH_ERROR:
      return { ...state, errors: state.errors.filter((error) => error.key !== action.payload.key) };

    default:
      return state;
  }
}
