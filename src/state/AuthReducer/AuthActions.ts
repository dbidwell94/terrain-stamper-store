import { authTypes, IAuthAction, IAuthState } from './types';
import { IThunkAction } from '../index';
import { v4 as uuid } from 'uuid';
import { ThunkDispatch } from 'redux-thunk';
import { CombinedState } from 'redux';
import { IUser } from 'src/api/userApi';
import { resetUserPurchasedStamps } from '@state/StampReducer/StampActions';
import api from '@api/index';
import { ApiError } from '@api/apiClient';
import { IStampAction } from '../StampReducer/types';

function timeoutError(
  errorKey: string,
  dispatch: ThunkDispatch<
    CombinedState<{
      authReducer: IAuthState;
    }>,
    null,
    IAuthAction
  >
) {
  setTimeout(() => {
    dispatch({ type: authTypes.REMOVE_AUTH_ERROR, payload: { key: errorKey, message: '' } });
  }, 5000);
}

export function toggleAuthModal(show: boolean): IAuthAction {
  return { type: authTypes.SET_AUTH_MODAL_OPEN, payload: show };
}

export function setCurrentUser(user: IUser | null): IAuthAction {
  return { type: authTypes.SET_CURRENT_USER, payload: user };
}

export function logUserOut(): IThunkAction<IAuthAction | IStampAction> {
  return function (dispatch) {
    localStorage.removeItem('token');
    dispatch({ type: authTypes.SET_TOKEN, payload: null });
    dispatch({ type: authTypes.SET_CURRENT_USER, payload: null });

    dispatch(resetUserPurchasedStamps());
  };
}

export function setToken(token: string | null): IAuthAction {
  return { type: authTypes.SET_TOKEN, payload: token };
}

export function logUserIn(username: string, password: string): IThunkAction<IAuthAction> {
  return async function (dispatch) {
    dispatch({ type: authTypes.SET_IS_AUTH_LOADING, payload: true });
    try {
      const user = await api.userApi.login(username, password);
      dispatch({ type: authTypes.SET_CURRENT_USER, payload: user });
      dispatch({ type: authTypes.SET_AUTH_MODAL_OPEN, payload: false });
    } catch (err) {
      const messageKey = uuid();
      if (err instanceof ApiError) {
        dispatch({
          type: authTypes.ADD_AUTH_ERROR,
          payload: { key: messageKey, message: err.message },
        });
      } else if (err instanceof Error) {
        dispatch({
          type: authTypes.ADD_AUTH_ERROR,
          payload: { key: messageKey, message: err.message || 'An auth error has occured' },
        });
      }
      timeoutError(messageKey, dispatch);
    } finally {
      dispatch({ type: authTypes.SET_IS_AUTH_LOADING, payload: false });
    }
  };
}

export function registerUser(username: string, email: string, password: string): IThunkAction<IAuthAction> {
  return async function (dispatch) {
    dispatch({ type: authTypes.SET_IS_AUTH_LOADING, payload: true });
    try {
      const user = await api.userApi.register({ email, password, username, roles: [] });
      dispatch({ type: authTypes.SET_CURRENT_USER, payload: user });
      dispatch({ type: authTypes.SET_AUTH_MODAL_OPEN, payload: false });
    } catch (err) {
      const messageKey = uuid();
      if (err instanceof ApiError) {
        dispatch({
          type: authTypes.ADD_AUTH_ERROR,
          payload: { key: messageKey, message: err.message },
        });
      } else if (err instanceof Error) {
        dispatch({
          type: authTypes.ADD_AUTH_ERROR,
          payload: { key: messageKey, message: err.message || 'An auth error has occured' },
        });
      }
      timeoutError(messageKey, dispatch);
    } finally {
      dispatch({ type: authTypes.SET_IS_AUTH_LOADING, payload: false });
    }
  };
}
