import { Action } from 'redux';
import { IServerCategory, IStampFile, IStampView } from 'src/api/stampApi';

export enum stampActionTypes {
  SET_STAMPS = 'SET_STAMPS',
  SET_IS_STAMPS_LOADING = 'SET_IS_STAMPS_LOADING',
  SET_LOADING_ERRORS = 'SET_LOADING_ERRORS',
  SET_CURRENTLY_LOADED_STAMP = 'SET_CURRENTLY_LOADED_STAMP',
  SET_USER_PURCHASED_STAMPS = 'SET_USER_PURCHASED_STAMPS',
  REFRESH_SERVER_CATEGORIES = 'REFRESH_SERVER_CATEGORIES',
  SET_CATEGORY_FILTER = 'SET_CATEGORY_FILTER',
}

export interface IStampState {
  stamps: IStampView[];
  stampsLoadingCalls: number;
  stampLoadingErrors: string[];
  currentlyOpenedStamp: IStampView | null;
  userPurchasedStamps: IStampView[];
  currentStampDownloads: IStampFile[];
  categories: IServerCategory[];
  currentFilteredCategories: number[];
}

interface SetStampAction extends Action {
  type: stampActionTypes.SET_STAMPS;
  payload: IStampView[];
}

interface GetPurchasedStampsAction extends Action {
  type: stampActionTypes.SET_USER_PURCHASED_STAMPS;
  payload: IStampView[];
}

interface LoadingAction extends Action {
  type: stampActionTypes.SET_IS_STAMPS_LOADING;
  payload: number;
}

interface LoadingErrorAction extends Action {
  type: stampActionTypes.SET_LOADING_ERRORS;
  payload: string[];
}

interface StampOpenAction extends Action {
  type: stampActionTypes.SET_CURRENTLY_LOADED_STAMP;
  payload: number | null;
}

interface SetCategoryFilterAction extends Action {
  type: stampActionTypes.SET_CATEGORY_FILTER;
  payload: number[];
}

interface RefreshCategoriesAction extends Action {
  type: stampActionTypes.REFRESH_SERVER_CATEGORIES;
  payload: IServerCategory[];
}

export type IStampAction =
  | SetStampAction
  | LoadingAction
  | LoadingErrorAction
  | StampOpenAction
  | GetPurchasedStampsAction
  | RefreshCategoriesAction
  | SetCategoryFilterAction;
