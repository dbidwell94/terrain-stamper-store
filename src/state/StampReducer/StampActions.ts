import { IStampAction, stampActionTypes } from './types';
import { IThunkAction } from '../index';
import { IStampView } from 'src/api/stampApi';
import api from '@api/index';

export function getStamps(): IThunkAction<IStampAction> {
  return async function (dispatch, getState) {
    dispatch({ type: stampActionTypes.SET_IS_STAMPS_LOADING, payload: getState().stampReducer.stampsLoadingCalls + 1 });

    try {
      const stamps = await api.stampApi.getStamps();

      dispatch({ type: stampActionTypes.SET_STAMPS, payload: stamps });
    } catch (err) {
      const loadingErrors = getState().stampReducer.stampLoadingErrors;

      if (err instanceof Error)
        dispatch({ type: stampActionTypes.SET_LOADING_ERRORS, payload: [...loadingErrors, err.message] });
    } finally {
      dispatch({
        type: stampActionTypes.SET_IS_STAMPS_LOADING,
        payload: getState().stampReducer.stampsLoadingCalls - 1,
      });
    }
  };
}

export function getUserPurchasedStamps(): IThunkAction<IStampAction> {
  return async function (dispatch, getState) {
    if (!getState().authReducer.token) return;
    try {
      dispatch({
        type: stampActionTypes.SET_IS_STAMPS_LOADING,
        payload: getState().stampReducer.stampsLoadingCalls + 1,
      });
      const stamps = await api.userApi.getPurchasedStamps();
      dispatch({ type: stampActionTypes.SET_USER_PURCHASED_STAMPS, payload: stamps });
    } catch (err) {
      const loadingErrors = getState().stampReducer.stampLoadingErrors;

      if (err instanceof Error)
        dispatch({ type: stampActionTypes.SET_LOADING_ERRORS, payload: [...loadingErrors, err.message] });
    } finally {
      dispatch({
        type: stampActionTypes.SET_IS_STAMPS_LOADING,
        payload: getState().stampReducer.stampsLoadingCalls - 1,
      });
    }
  };
}

export function resetUserPurchasedStamps(): IStampAction {
  return { type: stampActionTypes.SET_USER_PURCHASED_STAMPS, payload: [] };
}

export function addPurchasedStamp(stamp: IStampView): IThunkAction<IStampAction> {
  return async function (dispatch, getState) {
    const purchasedStamps = getState().stampReducer.userPurchasedStamps;
    dispatch({ type: stampActionTypes.SET_USER_PURCHASED_STAMPS, payload: [...purchasedStamps, stamp] });
  };
}

export function setCurrentlyViewedStamp(stampId: number | null): IStampAction {
  return { type: stampActionTypes.SET_CURRENTLY_LOADED_STAMP, payload: stampId };
}

export function refreshCategories(): IThunkAction<IStampAction> {
  return async function (dispatch) {
    try {
      const categories = await api.stampApi.getCategories();
      dispatch({
        type: stampActionTypes.REFRESH_SERVER_CATEGORIES,
        payload: categories,
      });
    } catch (err) {
      dispatch({
        type: stampActionTypes.REFRESH_SERVER_CATEGORIES,
        payload: [],
      });
    }
  };
}

export function updateCategoryFilter(categories: number[]): IThunkAction<IStampAction> {
  return function (dispatch, getState) {
    if (getState().stampReducer.currentFilteredCategories !== categories) {
      dispatch({
        payload: categories,
        type: stampActionTypes.SET_CATEGORY_FILTER,
      });
    }
  };
}
