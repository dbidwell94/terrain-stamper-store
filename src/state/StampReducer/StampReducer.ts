import { IStampState, IStampAction, stampActionTypes } from './types';

const initialState: IStampState = {
  stamps: [],
  stampsLoadingCalls: 0,
  stampLoadingErrors: [],
  currentlyOpenedStamp: null,
  userPurchasedStamps: [],
  currentStampDownloads: [],
  categories: [],
  currentFilteredCategories: [],
};

export default function (state = initialState, action: IStampAction): IStampState {
  switch (action.type) {
    case stampActionTypes.SET_STAMPS:
      return { ...state, stamps: action.payload };

    case stampActionTypes.SET_IS_STAMPS_LOADING:
      return { ...state, stampsLoadingCalls: action.payload };

    case stampActionTypes.SET_LOADING_ERRORS:
      return { ...state, stampLoadingErrors: action.payload };

    case stampActionTypes.SET_CURRENTLY_LOADED_STAMP:
      if (action.payload) {
        return { ...state, currentlyOpenedStamp: state.stamps.filter((stamp) => stamp.id === action.payload)[0] };
      }
      return { ...state, currentlyOpenedStamp: null };

    case stampActionTypes.SET_USER_PURCHASED_STAMPS:
      return { ...state, userPurchasedStamps: action.payload };

    case stampActionTypes.REFRESH_SERVER_CATEGORIES:
      return { ...state, categories: action.payload };

    case stampActionTypes.SET_CATEGORY_FILTER:
      return { ...state, currentFilteredCategories: action.payload };

    default:
      return state;
  }
}
