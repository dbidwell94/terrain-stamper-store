import { combineReducers, applyMiddleware, createStore, Middleware, AnyAction } from 'redux';
import 'react-redux';
import { ThunkAction } from 'redux-thunk';
import authReducer from './AuthReducer/AuthReducer';
import stampReducer from './StampReducer/StampReducer';
import packageReducer from './PackageReducer/PackageReducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

const globalState = combineReducers({ authReducer, stampReducer, packageReducer });

export type IGlobalState = ReturnType<typeof globalState>;

declare module 'react-redux' {
  // eslint-disable-next-line
  interface DefaultRootState extends IGlobalState {}
}

export type IThunkAction<T extends AnyAction> = ThunkAction<void, IGlobalState, null, T>;

const middlewares: Middleware[] = [thunk];
if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger);
}

export default createStore(globalState, applyMiddleware(...middlewares));
