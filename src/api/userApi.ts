import { decodeToken } from 'react-jwt';
import state from '@state/index';
import { setCurrentUser } from '@state/AuthReducer/AuthActions';
import ApiClient from '@api/apiClient';
import { IStampView } from '@api/stampApi';

export interface IRole {
  id: number;
  roleName: string;
}

export interface IUser {
  createdAt: string;
  updatedAt: string;
  username: string;
  email: string;
  id: number;
  roles: IRole[];
  purchases: IPurchase[];
}

export interface IUserView {
  id: number;
  username: string;
}

export interface IPurchase {
  id: number;
  paymentId: string;
  paymentStatus: PaymentStatus;
  stampId: number;
  userId: number;
}

export enum PaymentStatus {
  Failed = 'failed',
  Open = 'open',
  Paid = 'paid',
  Canceled = 'canceled',
  Expired = 'expired',
}

export interface ILoginResponse {
  token: string;
}

type IDecodedToken = Pick<IUser, 'id' | 'username'>;

export interface IRegisterOptions {
  username: string;
  email: string;
  password: string;
  roles: Omit<IRole, 'createdAt' | 'updatedAt' | 'id'>[];
}

export default class UserApiClient extends ApiClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  async login(username: string, password: string): Promise<IUser> {
    const [data] = await this.post<ILoginResponse>('/api/users/login', { username, password });
    this.saveJwt(data.token);
    const { id } = decodeToken(data.token) as IDecodedToken;
    return await this.getUserById(id);
  }

  async getUserById(id: number): Promise<IUser> {
    return (await this.get<IUser>(`/api/users/user/${id}`))[0];
  }

  async updateCurrentUser(): Promise<void> {
    if (this.token) {
      const { id } = decodeToken(this.token) as IDecodedToken;
      try {
        const user = await this.getUserById(id);
        state.dispatch(setCurrentUser(user));
      } catch (err) {
        this.deleteJwt();
      }
    }
  }

  async register(options: IRegisterOptions): Promise<IUser> {
    const [data] = await this.post<ILoginResponse>('/api/users/register', options);
    this.saveJwt(data.token);
    const { id } = decodeToken(data.token) as IDecodedToken;
    return await this.getUserById(id);
  }

  async getPurchasedStamps(): Promise<IStampView[]> {
    const [data] = await this.get<IStampView[]>('/api/payments/stamps/purchased');
    return data;
  }

  async sendForgotPasswordEmail(email: string): Promise<void> {
    await this.post('/api/users/sendforgotpasswordemail', { email });
  }

  async resetPasswordWithEmailToken(token: string, newPassword: string): Promise<void> {
    await this.post('/api/users/resetpassword', { linkHash: token, newPassword });
  }
}
