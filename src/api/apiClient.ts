import { setCurrentUser, setToken } from '@state/AuthReducer/AuthActions';
import state from '@state/index';
import ax, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import axios from 'axios';

type IApiConfig = AxiosRequestConfig;

export class ApiError extends Error {
  axiosError: AxiosError;
  constructor(message: string, axiosError: AxiosError) {
    super(message);
    this.axiosError = axiosError;
    Object.setPrototypeOf(ApiError.prototype, ApiError);
  }
}

export default abstract class ApiClient {
  private baseUrl: string;
  private axios: AxiosInstance;
  protected token: string | null;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
    this.axios = this.buildAxios();
    this.token = this.getJwtFromLocalStorage();
    state.subscribe(() => {
      if (state.getState().authReducer.token !== this.token) {
        this.token = state.getState().authReducer.token;
        this.axios = this.buildAxios();
      }
    });
  }

  protected buildAxios(tokenOverride?: string): AxiosInstance {
    const headers: Record<string, string> = {};
    if (tokenOverride) {
      headers.authorization = `bearer ${tokenOverride}`;
    } else if (this.getJwtFromLocalStorage()) {
      headers.authorization = `bearer ${this.getJwtFromLocalStorage()}`;
    }
    return ax.create({ baseURL: this.baseUrl, headers });
  }

  protected getJwtFromLocalStorage(): string | null {
    const token = localStorage.getItem('token');
    if (state.getState().authReducer.token !== token) {
      state.dispatch(setToken(token));
    }
    return token;
  }

  protected deleteJwt(): void {
    localStorage.removeItem('token');
    state.dispatch(setCurrentUser(null));
    state.dispatch(setToken(null));
    this.axios = this.buildAxios();
  }

  protected saveJwt(token: string): void {
    state.dispatch(setToken(token));
    localStorage.setItem('token', token);
    this.axios = this.buildAxios();
  }

  protected async get<T>(url: string, config?: IApiConfig): Promise<[T, AxiosResponse<T>]> {
    try {
      const res = await this.axios.get<T>(url, config);
      return [res.data, res];
    } catch (err) {
      if (axios.isAxiosError(err)) {
        if (err.response?.data?.error) {
          throw new ApiError(err.response.data.error[0], err);
        }
      }
      throw err;
    }
  }

  protected async post<T>(
    url: string,
    data?: Record<string, any>,
    config?: IApiConfig
  ): Promise<[T, AxiosResponse<T>]> {
    try {
      const res = await this.axios.post(url, data, config);
      return [res.data, res];
    } catch (err) {
      if (axios.isAxiosError(err)) {
        if (err.response?.data?.error) {
          throw new ApiError(err.response.data.error[0], err);
        }
      }
      throw err;
    }
  }

  protected async delete<T>(url: string, config?: IApiConfig): Promise<[T, AxiosResponse<T>]> {
    try {
      const res = await this.axios.delete(url, config);
      return [res.data, res];
    } catch (err) {
      if (axios.isAxiosError(err)) {
        if (err.response?.data?.error) {
          throw new ApiError(err.response.data.error[0], err);
        }
      }
      throw err;
    }
  }

  protected async put<T>(url: string, data: Record<string, any>, config?: IApiConfig): Promise<[T, AxiosResponse<T>]> {
    try {
      const res = await this.axios.put(url, data, config);
      return [res.data, res];
    } catch (err) {
      if (axios.isAxiosError(err)) {
        if (err.response?.data?.error) {
          throw new ApiError(err.response.data.error[0], err);
        }
      }
      throw err;
    }
  }
}
