import ApiClient from '@api/apiClient';
import { IPackage } from '@api/stampApi';

export interface IPackageEdit extends Omit<IPackage, 'id'> {
  saved: boolean;
  id: string | number;
}

export default class PackageApi extends ApiClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  /**
   * Submit a new package to the server.
   * @param pkg The package to submit
   * @returns The id the server has given the new package
   */
  async submitNewPackage(pkg: IPackageEdit): Promise<number> {
    const data = {
      packageName: pkg.name,
      description: pkg.description,
      stampIds: pkg.stamps.map((s) => s.id),
      discountMultiplier: pkg.discountMultiplier,
    };

    const [_, response] = await this.post<undefined>('/api/packages/create', data);
    const locationHeader: string = response.headers['location'];
    const splitHeader = locationHeader.split('/');
    const toReturn = Number(splitHeader[splitHeader.length - 1]);
    return toReturn;
  }

  async getAllPackages(): Promise<IPackage[]> {
    const [data] = await this.get<IPackage[]>('/api/packages/packages');
    return data;
  }

  async modifyPackage(pkg: IPackageEdit): Promise<void> {
    const data = {
      packageName: pkg.name,
      description: pkg.description,
      stampIds: pkg.stamps.map((s) => s.id),
      discountMultiplier: pkg.discountMultiplier,
    };
    await this.put(`/api/packages/package/${pkg.id}`, data);
  }

  async getPackageById(id: number): Promise<IPackage> {
    const [data] = await this.get<IPackage>(`api/packages/package/${id}`);

    return data;
  }

  async deletePackageById(id: number): Promise<void> {
    await this.delete(`/api/packages/package/${id}`);
  }

  async getUserPurchasedPackages(): Promise<IPackage[]> {
    const [data] = await this.get<IPackage[]>('/api/payments/packages/purchased');
    return data;
  }
}
