import ApiClient from '@api/apiClient';
import { IUserView } from '@api/userApi';

export interface IUploadFileArgs {
  stamp: File;
  stampName: string;
  stampType: string;
  categories: string[];
  price: number;
  progressEventCallback?: (arg: ProgressEvent) => void;
}

export interface ICategory {
  id: number;
  name: string;
}

export interface IPicture {
  id: number;
}

export interface IStampFile {
  resolution: string;
  isUnityPackage: boolean;
  id: number;
}

export interface IPackage {
  id: number;
  name: string;
  description: string;
  stamps: IStampView[];
  discountMultiplier: number;
}

export interface IStampView {
  id: number;
  isReleased: boolean;
  name: string;
  package: IPackage | null;
  pictures: IPicture[];
  price: number;
  releaseDate: string;
  stampType: string;
  categories: number[];
  uploadedUser: IUserView;
  files: IStampFile[];
}

export interface IStampEdit {
  name: string;
  categories: string[];
  stampType: string;
}

export interface IServerCategory {
  name: string;
  id: number;
  children: Record<string, IServerCategory>;
}

export default class StampApiClient extends ApiClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  async uploadStampFile(uploadOptions: IUploadFileArgs): Promise<void> {
    const { price, stamp, stampName, stampType, progressEventCallback } = uploadOptions;
    const data = new FormData();
    data.append('stamp', stamp);
    data.append('stampName', stampName);
    data.append('stampType', stampType);
    data.append('price', price.toString());
    for (const cat of uploadOptions.categories) {
      data.append('categories', cat);
    }

    await this.post('/api/stamps/stamp', data, {
      onUploadProgress: progressEventCallback,
      headers: { 'Content-Type': 'multipart/form-data' },
    });
  }

  async editStamp(stampId: number, stamp: IStampEdit): Promise<void> {
    await this.put<void>(`/api/stamps/stamp/${stampId}`, stamp);
  }

  async getStampById(id: number): Promise<IStampView> {
    const [data] = await this.get<IStampView>(`/api/stamps/stamp/${id}`);
    return data;
  }

  async getStamps(): Promise<IStampView[]> {
    const [data] = await this.get<IStampView[]>(`/api/stamps/all_stamps`);
    return data;
  }

  async getMyUploadedStamps(): Promise<IStampView[]> {
    const [data] = await this.get<IStampView[]>('/api/stamps/my_stamps');
    return data;
  }

  async getStampsByUserId(userId: number): Promise<IStampView[]> {
    const [data] = await this.get<IStampView[]>(`/api/stamps/stamps/byuser/${userId}`);
    return data;
  }

  async launchStampOnDate(stampId: number, releaseDate: Date): Promise<void> {
    await this.get<void>(`/api/stamps/release/${stampId}/${encodeURIComponent(releaseDate.toISOString())}`);
  }

  async getCategories(): Promise<IServerCategory[]> {
    const [res] = await this.get<Record<string, IServerCategory>>('/api/stamps/categories');

    return Object.values(res);
  }

  async getStampAmountByCategoryList(categories: number[]): Promise<number> {
    let res: number;

    if (categories.length < 1) {
      res = (await this.get<number>('/api/stamps/categories/amount'))[0];
    } else {
      res = (
        await this.get<number>(`/api/stamps/categories/amount?categoryList=${categories.join('&categoryList=')}`)
      )[0];
    }

    return res;
  }

  async getCategoriesByIds(ids: number[]): Promise<ICategory[]> {
    const route = '/api/stamps/categories/byids';

    const [res] = await this.post<ICategory[]>(route, ids);
    return res;
  }
}
