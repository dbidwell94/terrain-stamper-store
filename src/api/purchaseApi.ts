import ApiClient from '@api/apiClient';
import { IStampFile, IStampView } from '@api/stampApi';

export default class PurchaseApiClient extends ApiClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  async getLinkToPurchaseStamp(stampId: number): Promise<string> {
    const [data] = await this.get<string>(`/api/payments/stamp/${stampId}`);
    return data;
  }

  /**
   * Download a stamp file from the server using auth token to verify purchase
   * @param stampFile The file to download
   * @param onProgressCallback Should we receive a progress event callback?
   * @returns A blob URL to the file that has been downloaded (so the user can save to computer)
   */
  async downloadStampFile(
    stamp: IStampView,
    stampFile: IStampFile,
    onProgressCallback?: (progressEvent: ProgressEvent) => void
  ): Promise<[string, string]> {
    const [data] = await this.get<Blob>(
      `/api/stamp_files/stamp/${stamp.id}?fileResolution=${stampFile.resolution}&unityPackage=${stampFile.isUnityPackage}`,
      { onDownloadProgress: onProgressCallback, responseType: 'blob' }
    );

    (<any>data).lastModifiedDate = new Date();
    const fileName = `${stamp.name.replaceAll(' ', '-')}.${stampFile.isUnityPackage ? 'unitypackage' : 'zip'}`;
    const fileToReturn = new File([data], fileName);
    return [URL.createObjectURL(fileToReturn), fileName];
  }
}
