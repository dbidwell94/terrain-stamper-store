import UserApi from '@api/userApi';
import StampApi from '@api/stampApi';
import PurchaseApi from '@api/purchaseApi';
import PackageApi from '@api/packageApi';

export const serverUrl = localStorage.getItem('SERVER_URL') || 'https://backend.atlasterrain.com';

const api = {
  userApi: new UserApi(serverUrl),
  stampApi: new StampApi(serverUrl),
  packageApi: new PackageApi(serverUrl),
  purchaseApi: new PurchaseApi(serverUrl),
};

export default Object.freeze(api);
