import { IUser } from '../api/userApi';
import { isAdmin, arrayEquals } from '../utils';

describe('src/utils.ts', () => {
  it('isAdmin() functions as intended', async () => {
    const dummyUserAdmin = {
      roles: [
        {
          id: 1,
          roleName: 'admin',
        },
      ],
    } as unknown as IUser;
    const dummyUserUser = {
      roles: [
        {
          id: 2,
          roleName: 'user',
        },
      ],
    } as unknown as IUser;
    const dummyUserAdminUser = {
      roles: [
        {
          id: 2,
          roleName: 'user',
        },
        {
          id: 1,
          roleName: 'admin',
        },
      ],
    } as unknown as IUser;
    expect(isAdmin(null)).toBeFalsy();
    expect(isAdmin(dummyUserAdmin)).toBeTruthy();
    expect(isAdmin(dummyUserUser)).toBeFalsy();
    expect(isAdmin(dummyUserAdminUser)).toBeTruthy();
  });
  it('arrayEquals<T>(arr1: T[], arr2: T[]) functions as intended', async () => {
    const arr1 = [1, 2, 3, 4, 5];
    const arr2 = [1, 2, 3, 4, 5];
    expect(arrayEquals(arr1, arr2)).toBeTruthy();

    const arr3 = [2, 4, 6, 8];
    const arr4 = [1, 3, 5, 7];
    expect(arrayEquals(arr3, arr4)).toBeFalsy();

    const arr5 = [
      {
        id: 2,
        roleName: 'user',
      },
      {
        id: 1,
        roleName: 'admin',
      },
    ];
    const arr6 = [
      {
        id: 2,
        roleName: 'invalid',
      },
      {
        id: 1,
        roleName: 'admin',
      },
    ];
    expect(arrayEquals(arr5, arr6)).toBeFalsy();
    const arr7 = [
      {
        id: 2,
        roleName: 'user',
      },
      {
        id: 1,
        roleName: 'admin',
      },
    ];
    expect(arrayEquals(arr5, arr7)).toBeTruthy();
  });
});
