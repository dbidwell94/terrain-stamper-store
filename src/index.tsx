import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import App from '@components/App';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import state from './state';
import theme from './theme';
import './index.css';

// This is needed to accept hot reloads
if ((module as any).hot) {
  (module as any).hot.accept();
}

export const AtlasStore = () => (
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <Provider store={state}>
        <App />
      </Provider>
    </ThemeProvider>
  </React.StrictMode>
);

ReactDOM.render(<AtlasStore />, document.getElementById('root'));
