import React from 'react';
import styled from 'styled-components';

export const InputContainer = styled.div`
  position: relative;
  pointer-events: all;

  input {
    background: ${({ theme }) => theme.global.colors.inputBackground};
    width: 100%;
    height: 5rem;
    border: 0.2rem solid transparent;
    border: none;
    color: white;
    padding: ${({ theme }) => `${theme.global.padding / 2}rem ${theme.global.padding / 2}rem`};
    font-size: ${({ theme }) => theme.global.inputFontSize}pt;
    transition: 0.125s ease-in-out all;

    &:hover {
      border: 0.2rem solid gray;
    }

    &.error {
      border: thin solid red !important;
    }

    &:active,
    &:focus {
      border: 0.2rem solid ${({ theme }) => theme.global.colors.inputActive};
      outline: none;
      &.error {
        border: 0.2rem solid red !important;
      }
    }
    &:disabled {
      color: grey;
    }
    &[type='file'] {
      all: unset;
      font-size: ${({ theme }) => theme.global.inputFontSize}pt;
    }
  }
  span.error {
    width: 100%;
    padding: ${({ theme }) => `${theme.global.padding / 2}rem ${theme.global.padding / 2}rem`};
    display: flex;
    justify-content: center;
    position: absolute;
    align-items: center;
    width: fit-content;
    margin: 0.5rem 0rem;
    border: thin solid red;
    p {
      margin: 0;
      padding: 0;
      color: red;
      font-size: 1.5rem;
    }
  }
`;

type IInputProps = Omit<
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  'ref' | 'type'
> & {
  errorText?: string;
  type: inputTypes;
};

export enum inputTypes {
  date = 'date',
  email = 'email',
  file = 'file',
  image = 'image',
  number = 'number',
  password = 'password',
  submit = 'submit',
  text = 'text',
  url = 'url',
}

const Input = React.forwardRef<HTMLInputElement, IInputProps>((componentProps, ref) => {
  const { errorText, className, ...inputProps } = componentProps;

  return (
    <InputContainer className={className}>
      <input {...inputProps} ref={ref} className={errorText ? 'error' : ''} />
      {errorText && (
        <span className='error'>
          <p>{errorText}</p>
        </span>
      )}
    </InputContainer>
  );
});
Input.displayName = 'Input';

export default Input;
