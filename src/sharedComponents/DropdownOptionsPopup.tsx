import React from 'react';
import styled from 'styled-components';

interface IDropdownOptionsProps {
  options: string[];
  onChange?: (newValue: string) => void;
  enabled?: boolean;
}

const DropDownOptionsContainer = styled.div<{ enabled?: boolean }>`
  position: absolute;
  display: flex;
  height: ${({ enabled }) => (enabled ? 'auto' : '0rem')};
  flex-direction: column;
  align-items: center;
  width: 100%;
  top: 100%;
  left: 50%;
  background: black;
  transform: translate(-50%, 0%);
  overflow: hidden;
  div {
    cursor: pointer;
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: column;
    transition: 0.125s ease-in-out all;
    &:hover {
      background: #575757;
    }
  }
`;

export default function DropdownOptionsPopup(props: IDropdownOptionsProps) {
  const { options, enabled, onChange } = props;

  return (
    <DropDownOptionsContainer enabled={enabled}>
      {options.map((option, index) => (
        <div onClick={() => onChange && onChange(option)} key={`option-mapped-${index}-${option}`}>
          <p>{option}</p>
        </div>
      ))}
    </DropDownOptionsContainer>
  );
}
