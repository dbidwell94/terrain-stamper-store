import React from 'react';
import { Link, LinkProps } from 'react-router-dom';
import Button from './Button';
import styled from 'styled-components';

const StyledLink = styled(Link)`
  width: 100%;
  height: auto;
  text-align: center;
  display: flex;
  text-decoration: none;
  button {
    width: 100%;
  }
`;

type IButtonLinksProps = Omit<LinkProps, 'ref' | 'children'> & {
  displayText: string;
};

/**
 * Looks the same as a button, acts like a React Router Link
 */
const ButtonLink = React.forwardRef<HTMLAnchorElement, IButtonLinksProps>((props, ref) => {
  const { displayText, ...linkProps } = props;

  return (
    <StyledLink {...linkProps} ref={ref}>
      <Button className='dark square' fullWidth displayText={displayText} />
    </StyledLink>
  );
});
ButtonLink.displayName = 'ButtonLink';

export default ButtonLink;
