import React from 'react';
import styled from 'styled-components';
const CheckboxContainer = styled.div`
  width: 100%;
  background: #131313;
  display: flex;
  flex-wrap: wrap;

  .checkboxproperty {
    display: block;
    padding: ${({ theme }) => theme.global.padding}rem;
    padding-left: calc(25px + ${({ theme }) => theme.global.padding * 2}rem);
    position: relative;
    user-select: none;
    cursor: pointer;
    pointer-events: all;

    label {
      padding: ${({ theme }) => theme.global.padding}rem;
    }

    input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0 !important;
      width: 0 !important;
    }

    .checkmark {
      position: absolute;
      top: 50%;
      left: ${({ theme }) => theme.global.padding}rem;
      height: 25px;
      width: 25px;
      transform: translateY(-50%);
      background-color: #000000;
      pointer-events: none;

      &::after {
        content: '';
        position: absolute;
        display: none;
        left: 7px;
        top: 3px;
        width: 5px;
        height: 10px;
        border: solid #2196f3;
        border-width: 0 3px 3px 0;
        transform: rotate(45deg);
      }

      &.selected {
        border: 2px solid #2196f3;
        &::after {
          display: block;
        }
      }
    }
  }

  .checkboxproperty:hover input ~ .checkmark {
    border: 2px solid #2196f3;
  }
`;

interface ISelectOption {
  value: string;
  key: string;
  selected: boolean;
}

interface IOnChangeArgs {
  option: ISelectOption;
}

interface ICheckboxProps {
  options: ISelectOption[];
  className?: string;
  onChange?: (args: IOnChangeArgs) => void;
}

export default function CheckBox(props: ICheckboxProps) {
  const { className, options, onChange } = props;

  return (
    <CheckboxContainer className={className}>
      {options.map((option) => (
        <label className='checkboxproperty' key={option.key}>
          {option.value}
          <input
            id={option.key}
            type='checkbox'
            value={option.value}
            onClick={() => {
              if (!onChange) return;
              onChange({ option: { ...option, selected: !option.selected } });
            }}
            onChange={(e) => e.preventDefault()}
            checked={option.selected}
          />
          <span className={`checkmark ${option.selected ? 'selected' : ''}`}></span>
        </label>
      ))}
    </CheckboxContainer>
  );
}
