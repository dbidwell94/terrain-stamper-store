import React, { useState } from 'react';
import styled, { keyframes } from 'styled-components';
import CloseIcon from '@mui/icons-material/CloseOutlined';

interface IPillInputContainerProps {
  rounded?: boolean;
}

const PillExpand = keyframes`
  0% {
    padding: 0rem 0rem;
    max-width: 0%;
  }
  100% {
    padding: 0.25rem 0.75rem;
    max-width: 100%;
  }
`;

const PillInputContainer = styled.div<IPillInputContainerProps>`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  height: 5rem;
  background: ${({ theme }) => theme.global.colors.inputBackground};
  border: 0.2rem solid transparent;
  border-radius: ${({ rounded }) => (rounded ? '100rem' : '0rem')};
  transition: 0.125s ease-in-out all;
  pointer-events: all;

  &:hover {
    border: 0.2rem solid grey;
  }

  &.active {
    border: 0.2rem solid ${({ theme }) => theme.global.colors.inputActive};
  }

  div.pill-group {
    display: flex;
    flex-direction: row;
    align-items: center;
    div.pill {
      display: flex;
      justify-content: center;
      align-items: center;
      border: thin solid transparent;
      padding: 0rem 0rem;
      border-radius: ${({ rounded }) => (rounded ? '1rem' : '0rem')};
      background: ${({ theme }) => theme.global.colors.inputActive};
      color: black;
      margin: ${({ theme }) => theme.global.padding / 2}rem;
      margin-right: 0rem;
      max-width: 0%;
      overflow: hidden;
      animation: ${PillExpand} 0.25s forwards ease-in-out;
      small {
        font-size: 1.75rem;
        overflow: none;
      }
    }
  }

  input {
    font-size: 1.75rem;
    height: 100%;
    width: 100%;
    max-width: 100%;
    background: ${({ theme }) => theme.global.colors.inputBackground};
    outline: none;
    border: none;
    padding: ${({ theme }) => theme.global.padding / 2}rem;
    border-radius: ${({ rounded }) => (rounded ? '100rem' : '0rem')};
  }

  div#clear-input {
    position: absolute;
    width: 3rem;
    height: 3rem;
    left: calc(100% - 2rem);
    cursor: pointer;
    filter: opacity(0.25);
    transform: translate(-100%, 0%);
    transition: 0.125s ease-in-out all;
    &:hover {
      filter: opacity(1) blur(0.0625rem);
    }
  }
`;

interface IInputWithPillsProps {
  pills: string[];
  maxPills: number;
  errorText?: string;
  onChange: (value: string[]) => void;
  validationFunc?: (value: string) => boolean;
  rounded?: boolean;
  placeholder?: string;
}

export default function InputWithPills(props: IInputWithPillsProps) {
  const { validationFunc, pills, onChange, placeholder, maxPills, ...styleProps } = props;

  const [bufferText, setBufferText] = useState<string>('');
  const [isActive, setIsActive] = useState<boolean>(false);

  function onInputChange(evt: React.ChangeEvent<HTMLInputElement> | React.FocusEvent<HTMLInputElement>) {
    const { value } = evt.target;
    const { type } = evt;

    if (type === 'change') {
      const splitValue = value.split(/[,\s\n]/);
      if (splitValue.length < 2) {
        setBufferText(value);
      } else {
        const newValue = splitValue[0].trim();
        if (validationFunc) {
          if (!validationFunc(newValue)) return;
        }
        newValue && pills.length < maxPills && onChange([...pills, splitValue[0]]);

        setBufferText('');
      }
    } else {
      setIsActive(false);

      const newValue = value.trim();
      if (validationFunc) {
        if (!validationFunc(newValue)) return;
      }
      newValue && pills.length < maxPills && onChange([...pills, newValue]);

      setBufferText('');
    }
  }

  return (
    <PillInputContainer className={isActive ? 'active' : ''} {...styleProps}>
      <div className='pill-group'>
        {pills.map((p, i) => {
          return (
            <div className='pill' key={`input-pill-${p}-${i}`}>
              <small>{p}</small>
            </div>
          );
        })}
      </div>

      <input
        value={bufferText}
        placeholder={placeholder}
        onFocus={() => setIsActive(true)}
        onBlur={onInputChange}
        onKeyDown={(e) => {
          const { code } = e;
          if (code.toLocaleLowerCase() === 'backspace' && bufferText.length < 1 && pills.length > 0) {
            setBufferText(pills[pills.length - 1]);
            onChange(pills.slice(0, -1));
          }
        }}
        onChange={onInputChange}
      />
      <div id='clear-input'>
        <CloseIcon onClick={() => onChange([])} style={{ width: '100%', height: '100%' }} />
      </div>
    </PillInputContainer>
  );
}
