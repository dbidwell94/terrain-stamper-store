import React from 'react';
import styled from 'styled-components';

const ParentAuthContainer = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 2;
`;

interface IModalContainerProps {
  onClick: () => void;
}

const cancelModalId = 'cancel-modal-event-container';

const ModalContainer: React.FunctionComponent<IModalContainerProps> = (props) => {
  const children = (
    props.children
      ? () => {
          if (React.isValidElement(props.children)) {
            return React.cloneElement(props.children, {
              onClick: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => e.stopPropagation(),
            });
          } else return undefined;
        }
      : () => {
          return undefined;
        }
  )();

  return (
    <ParentAuthContainer
      id={cancelModalId}
      onClick={(e) => {
        e.preventDefault();
        if (e.currentTarget.id === cancelModalId) {
          props.onClick();
        }
      }}
    >
      {children}
    </ParentAuthContainer>
  );
};

export default ModalContainer;
