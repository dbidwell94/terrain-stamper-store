import React from 'react';
import styled from 'styled-components';

interface IStyleProps {
  borderColor?: string;
  invertColor?: boolean;
}

const ButtonContainer = styled.div<IStyleProps>`
  transition: 0.125s ease-in-out all;
  width: max-content;
  &.full-width {
    width: 100%;
    text-align: center;
  }

  &.full-height {
    height: 100%;
  }

  button {
    border-radius: ${({ theme }) => `${theme.global.padding / 1.5}rem`};
    transition: 0.125s ease-in-out all;
    background: gray;
    border: none;
    outline: none;
    font-size: 2rem;
    width: 100%;
    height: 100%;
    padding: ${({ theme }) => `${theme.global.padding / 5}rem`} ${({ theme }) => `${theme.global.padding}rem`};
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 12pt;
    color: ${({ invertColor }) => (invertColor ? 'black' : 'white')};
    pointer-events: all;

    &.dark {
      background: #252525;
      border: 1px solid #383838;
    }
    &.colored {
      background: #0087ff;
      //padding: ${({ theme }) => `${theme.global.padding / 5}rem`} ${({ theme }) => `${theme.global.padding}rem`};
    }
    &.thick {
      padding: ${({ theme }) => `${theme.global.padding / 2}rem`} ${({ theme }) => `${theme.global.padding}rem`};
    }
    &.huge {
      padding: ${({ theme }) => `${theme.global.padding}rem`} ${({ theme }) => `${theme.global.padding}rem`};
      border-radius: ${({ theme }) => `${theme.global.padding * 2}rem`};
    }
    &.important {
      background: #a83030;
    }
    &:hover {
      background: #575757;
      cursor: pointer;
      color: ${({ invertColor }) => (invertColor ? 'black' : 'white')};
    }
    &.square {
      border-radius: 0;
    }
    &:active {
      background: black;
      color: ${({ invertColor }) => (invertColor ? 'black' : 'white')};
    }
    &:disabled,
    &[disabled] {
      color: darkgrey;
      background: grey;
      &:hover {
        cursor: not-allowed;
      }
    }
  }
`;

interface IProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  displayText?: string;
  fullWidth?: boolean;
  fullHeight?: boolean;
  invertColor?: boolean;
  rootClassName?: string;
  arrowSize?: number;
}

type IButtonProps = IProps & IStyleProps;

/**
 * A nice, fancy styled Button with hover and press animations
 */
export default function Button(props: IButtonProps) {
  const {
    displayText,
    borderColor,
    fullWidth,
    invertColor,
    rootClassName,
    fullHeight,
    arrowSize: _,
    ...buttonProps
  } = props;

  function getClassName(): string {
    const names: string[] = [];
    if (fullWidth) names.push('full-width');
    if (buttonProps.disabled) names.push('disabled');
    if (rootClassName) names.push(rootClassName);
    if (fullHeight) names.push('full-height');

    return names.join(' ');
  }

  return (
    <ButtonContainer borderColor={borderColor} className={getClassName()} invertColor={invertColor}>
      <button {...buttonProps}>{displayText}</button>
    </ButtonContainer>
  );
}
