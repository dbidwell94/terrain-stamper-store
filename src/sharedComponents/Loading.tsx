import React from 'react';
import styled from 'styled-components';
import loadingSvg from '../assets/loadingSvg.svg';

const SvgContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  img {
    width: 20rem !important;
    height: 20rem !important;
    position: absolute !important;
    top: 50% !important;
    left: 50% !important;
    transform: translate(-50%, -50%) !important;
  }
`;

type LoadingSvgProps = Omit<
  React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>,
  'src' | 'ref'
>;

export default function LoadingSvg(props: LoadingSvgProps) {
  return (
    <SvgContainer>
      <img src={loadingSvg} {...props} />
    </SvgContainer>
  );
}
