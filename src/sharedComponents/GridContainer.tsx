import styled, { css } from 'styled-components';

interface IGridContainerProps {
  noPaddingVertical?: boolean;
  noPaddingHorizontal?: boolean;
  noScroll?: boolean;
}

const scroll = css`
  overflow-x: hidden;
  overflow-y: auto;
`;

const GridContainer = styled.div<IGridContainerProps>`
  padding: ${({ theme, noPaddingHorizontal, noPaddingVertical }) => {
    let builtString = '';
    builtString += noPaddingVertical ? 0 : theme.global.padding;
    builtString += 'rem ';
    builtString += noPaddingHorizontal ? 0 : theme.global.padding;
    builtString += 'rem';
    return builtString;
  }};
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(${({ theme }) => theme.global.stampViewSize}rem, 1fr));
  grid-gap: 1rem;
  width: 100%;
  height: auto;
  max-height: 100%;
  ${({ noScroll }) => !noScroll && scroll};

  &.loading {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    width: 100%;
  }
`;

export default GridContainer;
