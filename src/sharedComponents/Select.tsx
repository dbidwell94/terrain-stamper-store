import React from 'react';
import styled from 'styled-components';
import ArrowDown from '@mui/icons-material/KeyboardArrowDown';

const SelectContainer = styled.div`
  pointer-events: all;

  label {
    position: relative;

    select {
      background: #131313;
      border: none;
      color: white;
      height: 2.5rem;
      min-width: 30rem;
      appearance: none;
      font-size: ${({ theme }) => theme.global.inputFontSize}pt;
      padding: ${({ theme }) => `${theme.global.padding / 2}rem ${theme.global.padding / 2}rem`};
      text-transform: capitalize;

      option {
        background: black;
        color: white;
        text-transform: capitalize;
      }
    }

    .dropdownbuttonfake {
      z-index: -1;
      position: absolute;
      height: 100%;
      width: auto;
      background-position: right 0.5rem center;
      background-size: contain;
      opacity: 50%;
      right: 0;
      top: 0;
    }
  }
`;

interface ISelectOption {
  value: string;
  key: string;
}

type IStyledSelectProps = Omit<
  React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement>,
  'ref' | 'children'
> & {
  options: ISelectOption[];
  id: string;
};

const Select = React.forwardRef<HTMLSelectElement, IStyledSelectProps>((componentProps, ref) => {
  const { options, children: _, ...rest } = componentProps;

  return (
    <SelectContainer>
      <label htmlFor={rest.id}>
        <select {...rest} ref={ref}>
          {options.map((option) => (
            <option key={option.key}>{option.value}</option>
          ))}
        </select>
        <div className='dropdownbuttonfake'>
          <ArrowDown style={{ width: '100%', height: '100%' }} />
        </div>
      </label>
    </SelectContainer>
  );
});
Select.displayName = 'Select';

export default Select;
