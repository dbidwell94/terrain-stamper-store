declare module 'styled-components' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends ITheme {}
}

type ITheme = typeof theme;

const theme = {
  global: {
    padding: 2,
    borderSize: 0.125,
    stampViewSize: 28,
    inputFontSize: 12,
    colors: {
      stampBackground: '#00000024',
      inputBackground: '#131313',
      inputActive: '#0087ff',
    },
  },
};

export default theme;
