module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    '^.+\\.(js|jsx)$': 'babel-jest',
  },
  moduleNameMapper: {
    '^@src/(.*)': '<rootDir>/src/$1',
    '^@state/(.*)': '<rootDir>/src/state/$1',
    '^@components/(.*)': '<rootDir>/src/components/$1',
    '^@api/(.*)': '<rootDir>/src/api/$1',
    '^@sc/(.*)': '<rootDir>/src/sharedComponents/$1',
  },
};
